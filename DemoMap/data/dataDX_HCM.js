var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện Tai Mũi Họng Sài Gòn",
   "address": "Thôn 1, xã Phúc Lộc, TỈNH YÊN BÁI, tỉnh Yên Bái",
   "lat": 21.7305,
   "lon": 104.9104,
   "Number_of_beds": "Đặng Thị Minh",
    "area": "02033.819.115",
    "laixe": "Bùi Đức Toàn",
    "bacsi": "Nông Văn Dũng",
    "dieuduong": "Phạm Thái Dương"
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Y học cổ truyền",
  "address": "Phường Minh Tân, TỈNH YÊN BÁI, tỉnh Yên Bái",
   "lat": 21.7207,
   "lon": 104.9113,
   "Number_of_beds": " Nguyễn Đức Bằng",
    "area": "02033.670.297",
    "laixe": "Nguyễn Văn Sinh",
    "bacsi": "Nguyễn Văn Hùng",
    "dieuduong": "Phạm Thế Ninh"
 },
 // {
 //   "STT": 3,
 //   "Name": "Bệnh viện Quận 4",
 //   "address": "Bến Vân Đồn, Phường 9, Quận 4, TỈNH YÊN BÁI, Việt Nam",
 //   "lat": 10.76531,
 //   "lon": 106.70218,
 //   "Number_of_beds": "Vũ Duy Điều",
 //    "area": "0359.008.555",
 //    "laixe": "Ngô Quang Huynh",
 //    "bacsi": "Nguyễn Đức Cử",
 //    "dieuduong": "Nguyễn Thị Khánh"
 // }
];