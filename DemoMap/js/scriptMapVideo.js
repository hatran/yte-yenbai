﻿
var lat = 10.773448; 
var lon = 106.705328;

var map = new L.Map('map', {

        zoom: 18,
        minZoom: 4,
});



    // create a new tile layer
    var tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl,
    {
        attribution: '',//'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 18
    });

    map.addLayer(layer);

    map.setView([lat, lon], 16);

    var FireIcon = L.icon({
        iconUrl: 'images/fire-2-32_2.gif',
        iconSize: [36, 36], // size of the icon
    });

    var marker = L.marker([lat, lon], { icon: FireIcon }).addTo(map);


    // var videoUrls = [
    //     'videos/ChayQ8.mp4'
    // ];

    // var bounds = L.latLngBounds([[ 10, 106], [ 10, 105]]);

    // var videoOverlay = L.videoOverlay( videoUrls, bounds, {
    //     opacity: 0.8
    // }).addTo(map);

    // videoOverlay.on('load', function () {
    //     var MyPauseControl = L.Control.extend({
    //         onAdd: function() {
    //             var button = L.DomUtil.create('button');
    //             button.innerHTML = '⏸';
    //             L.DomEvent.on(button, 'click', function () {
    //                 videoOverlay.getElement().pause();
    //             });
    //             return button;
    //         }
    //     });
    //     var MyPlayControl = L.Control.extend({
    //         onAdd: function() {
    //             var button = L.DomUtil.create('button');
    //             button.innerHTML = '⏵';
    //             L.DomEvent.on(button, 'click', function () {
    //                 videoOverlay.getElement().play();
    //             });
    //             return button;
    //         }
    //     });

    //     var pauseControl = (new MyPauseControl()).addTo(map);
    //     var playControl = (new MyPlayControl()).addTo(map);
    // });
    // 

var CCIcon = L.icon({
        iconUrl: 'images/icons/icon_camera.png',
        iconSize: [32, 32], // size of the icon
    });
// var xethang1 = [[10.773583, 106.705704], [10.773583, 106.705704]];
// var xethang2 = [[10.773004, 106.705198], [10.773004, 106.705198]];
// var xethang5 = [[10.773249, 106.705557], [10.773249, 106.705557]];
// var xethang6 = [[10.773632, 106.705110], [10.773632, 106.705110]];

// var marker1 = L.Marker.movingMarker(xethang1,
//             [40000, 12000, 40000], { autostart: false, loop: false, icon: CCIcon }).addTo(map);

//         marker1.loops = 0;
//         marker1.bindPopup('<video width="200" height="150" controls autoplay muted><source src="videos/ChayQ8.mp4" type="video/mp4"></video>')
//         marker1.openPopup();

// var marker2 = L.Marker.movingMarker(xethang2,
//             [50000, 40000, 40000, 50000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

//         marker2.loops = 0;
//         marker2.bindPopup('<video width="200" height="150" controls autoplay muted><source src="videos/ChayQ8.mp4" type="video/mp4"></video>')
//         marker2.openPopup();
//         
var popupLocation1 = new L.LatLng(10.773583, 106.708500);
var popupLocation2 = new L.LatLng(10.773583, 106.702000);
var popupLocation3 = new L.LatLng(10.770000, 106.702000);
var popupLocation4 = new L.LatLng(10.770000, 106.708500);

var popupContent1 = '<video width="200" height="150" controls autoplay muted><source src="videos/ChayQ8.mp4" type="video/mp4"></video>',
popup1 = new L.Popup();

popup1.setLatLng(popupLocation1);
popup1.setContent(popupContent1);

var popupContent2 = '<video width="200" height="150" controls autoplay muted><source src="videos/ChayQ8.mp4" type="video/mp4"></video>',
popup2 = new L.Popup();

popup2.setLatLng(popupLocation2);
popup2.setContent(popupContent2);

var popupContent3 = '<video width="200" height="150" controls autoplay muted><source src="videos/ChayQ8.mp4" type="video/mp4"></video>',
popup3 = new L.Popup();

popup3.setLatLng(popupLocation3);
popup3.setContent(popupContent3);

var popupContent4 = '<video width="200" height="150" controls autoplay muted><source src="videos/ChayQ8.mp4" type="video/mp4"></video>',
popup4 = new L.Popup();

popup4.setLatLng(popupLocation4);
popup4.setContent(popupContent4);

marker.on('mouseover', function() {
           map.addLayer(popup1).addLayer(popup2).addLayer(popup3).addLayer(popup4);
        });

