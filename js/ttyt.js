var datattyt = [
 {
   "STT": 1,
   "Name": " Trung tâm y tế quận Hoàn Kiếm.",
   "address": "36 Phố Ngô Quyền, Hàng Bài, Hoàn Kiếm, YÊN BÁI, Việt Nam",
   "Longtitude": 21.0290521,
   "Latitude": 105.8475411
 },
 {
   "STT": 2,
   "Name": " Trung tâm y tế quận Ba Đình.",
   "address": "101 Quán Thánh, Ba Đình, YÊN BÁI, Việt Nam",
   "Longtitude": 21.0405311,
   "Latitude": 105.8289584
 },
 {
   "STT": 3,
   "Name": " Trung tâm y tế quận Đống Đa.",
   "address": "10 Ngõ 122 - Láng, Thịnh Quang, Đống Đa, YÊN BÁI, Việt Nam",
   "Longtitude": 21.0406175,
   "Latitude": 105.7983161
 },
 {
   "STT": 4,
   "Name": " Trung tâm y tế quận Hai Bà Trưng.",
   "address": "16 Phạm Đình Hổ, Hai Bà Trưng, YÊN BÁI, Việt Nam",
   "Longtitude": 21.0164816,
   "Latitude": 105.8549515
 },
 {
   "STT": 5,
   "Name": " Trung tâm y tế quận Thanh Xuân.",
   "address": "23 Ngõ 282 Đường Khương Đình, Hạ Đình, Thanh Xuân, YÊN BÁI, Việt Nam",
   "Longtitude": 20.9908419,
   "Latitude": 105.8101737
 },
 {
   "STT": 6,
   "Name": " Trung tâm y tế quận Cầu Giấy.",
   "address": "18 Phạm Hùng, Mai Dịch, Cầu Giấy, YÊN BÁI, Việt Nam",
   "Longtitude": 21.0338698,
   "Latitude": 105.7769289
 },
 {
   "STT": 7,
   "Name": " Trung tâm y tế quận Tây Hồ.",
   "address": "710b Lạc Long Quân, Phú Thượng, Tây Hồ, YÊN BÁI, Việt Nam",
   "Longtitude": 21.0796893,
   "Latitude": 105.8139774
 },
 {
   "STT": 8,
   "Name": " Trung tâm y tế quận Hoàng Mai.",
   "address": "5 Bùi Huy Bích, Hoàng Liệt, Hoàng Mai, YÊN BÁI, Việt Nam",
   "Longtitude": 20.9681674,
   "Latitude": 105.8444634
 },
 {
   "STT": 9,
   "Name": " Trung tâm y tế quận Long Biên.",
   "address": "Khu đô thị Việt Hưng, Long Biên, YÊN BÁI, Việt Nam",
   "Longtitude": 21.068615,
   "Latitude": 105.9080833
 },
 {
   "STT": 10,
   "Name": " Trung tâm y tế quận Hà Đông.",
   "address": "57 Đường Tô Hiệu, P. Nguyễn Trãi, Hà Đông, YÊN BÁI, Việt Nam",
   "Longtitude": 20.9676862,
   "Latitude": 105.7769164
 },
 {
   "STT": 11,
   "Name": " Trung tâm y tế quận Nam Từ Liêm.",
   "address": "3 Đường Liên Cơ, Mỹ Đình, Nam Từ Liêm, YÊN BÁI, Việt Nam",
   "Longtitude": 21.0274008,
   "Latitude": 105.7796974
 },
 {
   "STT": 12,
   "Name": " Trung tâm y tế quận Bắc Từ Liêm.",
   "address": "CT5A, khu Tái định cư Kiều Mai, Phường Phúc Diễn, Quận Bắc Từ Liêm, YÊN BÁI",
   "Longtitude": 21.0433643,
   "Latitude": 105.752117
 },
 {
   "STT": 13,
   "Name": " Trung tâm y tế huyện Gia Lâm.",
   "address": "1 Ngô Xuân Quảng, Trâu Quỳ, Gia Lâm, YÊN BÁI, Việt Nam",
   "Longtitude": 21.0204407,
   "Latitude": 105.9358944
 },
 {
   "STT": 14,
   "Name": " Trung tâm y tế huyện Đông Anh.",
   "address": "Đường 23B, Tiên Dương, Đông Anh, YÊN BÁI, Việt Nam",
   "Longtitude": 21.1454904,
   "Latitude": 105.8371219
 },
 {
   "STT": 15,
   "Name": " Trung tâm y tế huyện Thanh Trì.",
   "address": "118 Tứ Hiệp, Văn Điển, Thanh Trì, YÊN BÁI, Việt Nam",
   "Longtitude": 20.9483695,
   "Latitude": 105.8440232
 },
 {
   "STT": 16,
   "Name": " Trung tâm y tế huyện Sóc Sơn.",
   "address": "Phù Linh, Sóc Sơn, YÊN BÁI, Việt Nam",
   "Longtitude": 21.2628901,
   "Latitude": 105.8472519
 },
 {
   "STT": 17,
   "Name": " Trung tâm y tế huyện Ba Vì.",
   "address": "TT. Tây Đằng, Ba Vì, YÊN BÁI, Việt Nam",
   "Longtitude": 21.2067479,
   "Latitude": 105.4251206
 },
 {
   "STT": 18,
   "Name": " Trung tâm y tế huyện Phúc Thọ.",
   "address": "TT. Phúc Thọ, Phúc Thọ, YÊN BÁI, Việt Nam",
   "Longtitude": 21.1059624,
   "Latitude": 105.5351353
 },
 {
   "STT": 19,
   "Name": " Trung tâm y tế huyện Thạch Thất.",
   "address": "ĐT419, Bình Phú, Thạch Thất, YÊN BÁI, Việt Nam",
   "Longtitude": 21.0237712,
   "Latitude": 105.6024715
 },
 {
   "STT": 20,
   "Name": " Trung tâm y tế huyện Quốc Oai.",
   "address": "Đồng Hương, TT. Quốc Oai, Quốc Oai, YÊN BÁI, Việt Nam",
   "Longtitude": 20.9930807,
   "Latitude": 105.6419134
 },
 {
   "STT": 21,
   "Name": " Trung tâm y tế huyện Đan Phượng.",
   "address": "Huyện Đan Phương Huyện Đan Phượng, Tân Hội, Đan Phượng, YÊN BÁI, Việt Nam",
   "Longtitude": 21.0869839,
   "Latitude": 105.6701195
 },
 {
   "STT": 22,
   "Name": " Trung tâm y tế huyện Hoài Đức.",
   "address": "Trạm Y tế xã Đức Giang, Đức Giang, Hoài Đức, YÊN BÁI, Việt Nam",
   "Longtitude": 21.0587882,
   "Latitude": 105.7031323
 },
 {
   "STT": 23,
   "Name": " Trung tâm y tế huyện Chương Mỹ.",
   "address": "Tt Chúc Sơn Huyện Chương Mỹ, TT. Chúc Sơn, Chương Mỹ, YÊN BÁI, Việt Nam",
   "Longtitude": 20.9257971,
   "Latitude": 105.685336
 },
 {
   "STT": 24,
   "Name": " Trung tâm y tế huyện Thanh Oai.",
   "address": "huyện Thanh Oai, TT. Kim Bài, Thanh Oai, YÊN BÁI, Việt Nam",
   "Longtitude": 20.9259824,
   "Latitude": 105.6328045
 },
 {
   "STT": 25,
   "Name": " Trung tâm y tế huyện Mỹ Đức.",
   "address": "Đại Nghĩa, TT. Đại Nghĩa, Mỹ Đức, YÊN BÁI, Việt Nam",
   "Longtitude": 20.6831007,
   "Latitude": 105.7418123
 },
 {
   "STT": 26,
   "Name": " Trung tâm y tế huyện Thường Tín.",
   "address": "ĐT73, Tô Hiệu, Thường Tín, YÊN BÁI, Việt Nam",
   "Longtitude": 20.8061424,
   "Latitude": 105.8845024
 },
 {
   "STT": 27,
   "Name": " Trung tâm y tế huyện Phú Xuyên.",
   "address": "QL1A, TT. Phú Xuyên, Phú Xuyên, YÊN BÁI, Việt Nam",
   "Longtitude": 20.7435881,
   "Latitude": 105.9139196
 },
 {
   "STT": 28,
   "Name": " Trung tâm y tế huyện Ứng Hòa.",
   "address": "Thanh Ấm, TT. Vân Đình, Ứng Hòa, YÊN BÁI, Việt Nam",
   "Longtitude": 20.7165033,
   "Latitude": 105.7863726
 },
 {
   "STT": 29,
   "Name": " Trung tâm y tế huyện Mê Linh.",
   "address": "Đại Thinh, Mê Linh, YÊN BÁI, Việt Nam",
   "Longtitude": 21.1839475,
   "Latitude": 105.7217905
 },
 {
   "STT": 30,
   "Name": " Trung tâm y tế thị xã Sơn Tây",
   "address": "1 Lê Lợi, P. Quang Trung, Sơn Tây, YÊN BÁI, Việt Nam",
   "Longtitude": 21.1414813,
   "Latitude": 105.5039103
 }
];