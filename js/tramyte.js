var datatramyte = [
 {
   "STT": 1,
   "Name": "Trạm y tế phường Yên Ninh",
   "address": "Phường Yên Ninh - TP Yên Bái - tỉnh Yên Bái",
   "Longtitude": 21.7071333,
   "Latitude": 104.8720476
 },
 {
   "STT": 2,
   "Name": "Trạm y tế phường Minh Tân",
   "address": "Phường Minh Tân - TP Yên Bái - tỉnh Yên Bái",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 3,
   "Name": "Trạm y tế phường Nguyễn Thái Học",
   "address": "Phường Nguyễn Thái Học - TP Yên Bái - Tỉnh Yên Bái",
   "Longtitude": 21.7119663,
   "Latitude": 104.8790296
 },
 {
   "STT": 4,
   "Name": "Trạm y tế phường Đồng Tâm",
   "address": "Phường Đồng Tâm - TP Yên Bái - tỉnh Yên Bái",
   "Longtitude": 21.7190744,
   "Latitude": 104.9126044
 },
 {
   "STT": 5,
   "Name": "Trạm y tế phường Hồng Hà",
   "address": "Phường Hồng Hà - TP Yên Bái - tỉnh Yên Bái",
   "Longtitude": 21.6976918,
   "Latitude": 104.8761104
 },
 {
   "STT": 6,
   "Name": "Trạm y tế phường Pú Trạng",
   "address": "Phường Pú Trạng - Thị xã Nghĩa Lộ - Tỉnh Yên Bái",
   "Longtitude": 21.599737,
   "Latitude": 104.492712
 },
 {
   "STT": 7,
   "Name": "Trạm y tế phường Trung Tâm",
   "address": "Phường Trung Tâm - Thị xã Nghĩa Lộ - Tỉnh Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 8,
   "Name": "Trạm y tế phường Tân An",
   "address": "Phường Tân An - Thị xã Nghĩa Lộ - Tỉnh Yên Bái",
   "Longtitude": 21.5928826,
   "Latitude": 104.50436
 },
 {
   "STT": 9,
   "Name": "Trạm y tế phường Cầu Thia",
   "address": "Phường Cầu Thia - Thị xã Nghĩa Lộ - Tỉnh Yên Bái",
   "Longtitude": 21.5873546,
   "Latitude": 104.5174652
 },
 {
   "STT": 10,
   "Name": "Trạm y tế Thị trấn Mù Căng Chải",
   "address": "Thị Trấn Mù Căng Chải - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.8528313,
   "Latitude": 104.0836104
 },
 {
   "STT": 11,
   "Name": "Trạm y tế xã Cổ Phúc",
   "address": "Thị trấn Cổ Phúc - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.7625049,
   "Latitude": 104.8191965
 },
 {
   "STT": 12,
   "Name": "Trạm y tế TT Yên Bình",
   "address": "Thị Trấn Yên Bình - Huyện Yên Bình",
   "Longtitude": 21.7459652,
   "Latitude": 104.9680914
 },
 {
   "STT": 13,
   "Name": "Trạm y tế xã Tân Đồng",
   "address": "Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.6160083,
   "Latitude": 104.798771
 },
 {
   "STT": 14,
   "Name": "Trạm y tế Púng Luông",
   "address": "Xã Púng Luông - Huyện Mù Cang Chải",
   "Longtitude": 21.7514895,
   "Latitude": 104.1742045
 },
 {
   "STT": 15,
   "Name": "Trạm y tế xã Cát Thịnh",
   "address": "Xã Cát Thịnh - Huyện Văn Chấn",
   "Longtitude": 21.4508836,
   "Latitude": 104.6937674
 },
 {
   "STT": 16,
   "Name": "Trạm y tế TT Trạm Tấu",
   "address": "Thị Trấn Trạm Têu - Huyện Trạm Têu",
   "Longtitude": 21.5148504,
   "Latitude": 104.4315746
 },
 {
   "STT": 17,
   "Name": "Trạm y tế xã Cảm Nhân",
   "address": "Xã Cảm Nhân - Huyện Yên Bình",
   "Longtitude": 21.9715666,
   "Latitude": 104.9724305
 },
 {
   "STT": 18,
   "Name": "Trạm y tế phường Nguyễn Phúc",
   "address": "Phường Nguyễn Phúc - TP Yên Bái - tỉnh Yên Bái",
   "Longtitude": 21.7069492,
   "Latitude": 104.8702721
 },
 {
   "STT": 19,
   "Name": "Trạm y tế phường Yên Thịnh",
   "address": "Phường Yên Thịnh - TP Yên Bái - tỉnh Yên Bái",
   "Longtitude": 21.7291001,
   "Latitude": 104.9301246
 },
 {
   "STT": 20,
   "Name": "Trạm y tế XN ĐM Hà Lào",
   "address": "TP Yên Bái",
   "Longtitude": 21.7167689,
   "Latitude": 104.8985878
 },
 {
   "STT": 21,
   "Name": "Trạm y tế phường Nam Cường",
   "address": "Phường Nam Cường - TP Yên Bái - tỉnh Yên Bái",
   "Longtitude": 21.7071333,
   "Latitude": 104.8720476
 },
 {
   "STT": 22,
   "Name": "Trạm y tế xã Tuy Lộc",
   "address": "Trạm y tế xã Tuy Lộc - TP Yên Bái - tỉnh Yên Bái",
   "Longtitude": 21.6975069,
   "Latitude": 104.8428317
 },
 {
   "STT": 23,
   "Name": "Trạm y tế xã Tân Thịnh",
   "address": "Trạm y tế xã Tân Thịnh - TP Yên Bái - tỉnh Yên Bái",
   "Longtitude": 21.7111091,
   "Latitude": 104.9358522
 },
 {
   "STT": 24,
   "Name": "Trạm y tế xã Minh Bảo",
   "address": "Xã Minh Bảo - TP Yên Bái - tỉnh Yên Bái",
   "Longtitude": 21.7513284,
   "Latitude": 104.9096846
 },
 {
   "STT": 25,
   "Name": "Trạm y tế xã Vĩnh Lạc",
   "address": "Xã Vĩnh Lạc - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.0951279,
   "Latitude": 104.8396248
 },
 {
   "STT": 26,
   "Name": "Trạm y tế xã Mai Sơn",
   "address": "Xã Mai Sơn - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.1873306,
   "Latitude": 104.7229279
 },
 {
   "STT": 27,
   "Name": "Trạm y tế xã Yên Thế",
   "address": "Thị trấn Yên Thế - Huyện Lục Yên - Tỉnh Yên Bái",
   "Longtitude": 22.0899425,
   "Latitude": 104.7695962
 },
 {
   "STT": 28,
   "Name": "Trạm y tế xã Mường Lai",
   "address": "Xã Mường Lai - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.1596549,
   "Latitude": 104.8337879
 },
 {
   "STT": 29,
   "Name": "Trạm y tế xã Tân Lĩnh",
   "address": "Xã Tân Lĩnh - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.123854,
   "Latitude": 104.7170954
 },
 {
   "STT": 30,
   "Name": "Trạm y tế xã Trúc Lâu",
   "address": "Xã Tróc Lâu - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.037119,
   "Latitude": 104.6471221
 },
 {
   "STT": 31,
   "Name": "Trạm y tế xã Yên Thắng",
   "address": "Xã Yên Thắng - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.1269975,
   "Latitude": 104.7462603
 },
 {
   "STT": 32,
   "Name": "Trạm y tế xã Minh Xuân",
   "address": "Xã Minh Xuân - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.1406957,
   "Latitude": 104.7871004
 },
 {
   "STT": 33,
   "Name": "Trạm y tế xã Liễu Đô",
   "address": "Xã Liôu Đô - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.0867224,
   "Latitude": 104.8046066
 },
 {
   "STT": 34,
   "Name": "Trạm y tế xã Tô Mậu",
   "address": "Xã Tô Mậu - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.1344753,
   "Latitude": 104.6646125
 },
 {
   "STT": 35,
   "Name": "Trạm y tế xã Tân Phượng",
   "address": "Xã Tân Phượng - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.2487968,
   "Latitude": 104.6238048
 },
 {
   "STT": 36,
   "Name": "Trạm y tế xã Phan Thanh",
   "address": "Xã Phan Thanh - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 21.9947088,
   "Latitude": 104.7929356
 },
 {
   "STT": 37,
   "Name": "Trạm y tế xã Phúc Lợi",
   "address": "Xã Phúc Lợi - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.0095926,
   "Latitude": 104.6937674
 },
 {
   "STT": 38,
   "Name": "Trạm y tế xã Lâm Thượng",
   "address": "Xã Lâm Thượng - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.2212452,
   "Latitude": 104.670443
 },
 {
   "STT": 39,
   "Name": "Trạm y tế xã Khánh Thiện",
   "address": "Xã Khánh Thiện - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.2360289,
   "Latitude": 104.6995991
 },
 {
   "STT": 40,
   "Name": "Trạm y tế xã Minh Chuẩn",
   "address": "Xã Minh Chuẩn - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.1831695,
   "Latitude": 104.6412925
 },
 {
   "STT": 41,
   "Name": "Trạm y tế xã An Lạc",
   "address": "Xã An Lạc - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.1344972,
   "Latitude": 104.6004911
 },
 {
   "STT": 42,
   "Name": "Trạm y tế xã Khai Trung",
   "address": "Xã Khai Trung - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.1799706,
   "Latitude": 104.6762738
 },
 {
   "STT": 43,
   "Name": "Trạm y tế xã Khánh Hoà",
   "address": "Xã Khánh Hoà - huyện Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.0879244,
   "Latitude": 104.6004911
 },
 {
   "STT": 44,
   "Name": "Trạm y tế xã Tân Lập",
   "address": "Xã Tân Lập - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.0561022,
   "Latitude": 104.7579278
 },
 {
   "STT": 45,
   "Name": "Trạm y tế xã Trung Tâm",
   "address": "Xã Trung Tâm - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 21.9799257,
   "Latitude": 104.7637619
 },
 {
   "STT": 46,
   "Name": "Trạm y tế xã An Phú",
   "address": "Xã An Phú - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 21.9967745,
   "Latitude": 104.8337879
 },
 {
   "STT": 47,
   "Name": "Trạm y tế xã Động Quan",
   "address": "Xã Đồng Quan - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.0815602,
   "Latitude": 104.670443
 },
 {
   "STT": 48,
   "Name": "Trạm y tế xã Minh Tiến",
   "address": "Xã Minh Tiến -  Lục Yên",
   "Longtitude": 22.0644264,
   "Latitude": 104.8571368
 },
 {
   "STT": 49,
   "Name": "Trạm y tế xã Xuân ái",
   "address": "Xã Xuân ái - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.8148923,
   "Latitude": 104.7229279
 },
 {
   "STT": 50,
   "Name": "Trạm y tế xã An Thịnh",
   "address": "Xã An Thịnh - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.8804826,
   "Latitude": 104.6412925
 },
 {
   "STT": 51,
   "Name": "Trạm y tế xã Mỏ Vàng",
   "address": "Xã Má Vàng - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.8698428,
   "Latitude": 104.5655273
 },
 {
   "STT": 52,
   "Name": "Trạm y tế xã Châu Quế Hạ",
   "address": "Xã Châu Quế Hạ - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.992582,
   "Latitude": 104.4956239
 },
 {
   "STT": 53,
   "Name": "Trạm y tế xã Nà Hẩu",
   "address": "Xã Nà Hèu - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.7639955,
   "Latitude": 104.577181
 },
 {
   "STT": 54,
   "Name": "Trạm y tế xã Lâm Giang",
   "address": "Xã Lâm Giang - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 22.0730646,
   "Latitude": 104.5072722
 },
 {
   "STT": 55,
   "Name": "Trạm y tế xã Yên Phú",
   "address": "Xã Yên Phú - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.8307558,
   "Latitude": 104.6762738
 },
 {
   "STT": 56,
   "Name": "Trạm y tế xã Tân Hợp",
   "address": "Xã Tân Hợp - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.8804401,
   "Latitude": 104.577181
 },
 {
   "STT": 57,
   "Name": "Trạm y tế xã Đại Phác",
   "address": "Xã Đại Phác - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.8455566,
   "Latitude": 104.6412925
 },
 {
   "STT": 58,
   "Name": "Trạm y tế xã Đại Sơn",
   "address": "Xã Đại Sơn - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.835953,
   "Latitude": 104.5538744
 },
 {
   "STT": 59,
   "Name": "Trạm y tế xã Viễn Sơn",
   "address": "Xã Viôn Sơn - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.7767934,
   "Latitude": 104.6937674
 },
 {
   "STT": 60,
   "Name": "Trạm y tế xã Yên Hợp",
   "address": "Xã Yên Hợp - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.8519248,
   "Latitude": 104.6995991
 },
 {
   "STT": 61,
   "Name": "Trạm y tế Thị trấn Mậu A",
   "address": "Thị Trấn Mậu A - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.8783788,
   "Latitude": 104.6966832
 },
 {
   "STT": 62,
   "Name": "Trạm y tế xã Yên Hưng",
   "address": "Xã Yên Hưng - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.856683,
   "Latitude": 104.7433436
 },
 {
   "STT": 63,
   "Name": "Trạm y tế xã Yên Thái",
   "address": "Xã Yên Thái - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.8868277,
   "Latitude": 104.7637619
 },
 {
   "STT": 64,
   "Name": "Trạm y tế xã Mậu Đông",
   "address": "Xã Mậu Đông - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.920702,
   "Latitude": 104.6471221
 },
 {
   "STT": 65,
   "Name": "Trạm y tế xã Ngòi A",
   "address": "Xã Ngũi A - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.9143532,
   "Latitude": 104.7170954
 },
 {
   "STT": 66,
   "Name": "Trạm y tế xã Đông An",
   "address": "Xã Đông An - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.9524064,
   "Latitude": 104.5538744
 },
 {
   "STT": 67,
   "Name": "Trạm y tế xã Đông Cuông",
   "address": "Xã Đông Cuông - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.9534985,
   "Latitude": 104.6063192
 },
 {
   "STT": 68,
   "Name": "Trạm y tế xã Lang Thíp",
   "address": "Xã Lang Thíp - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 22.1492424,
   "Latitude": 104.4373961
 },
 {
   "STT": 69,
   "Name": "Trạm y tế xã Châu Quế Thượng",
   "address": "Xã Châu Quế Thượng - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 22.0581316,
   "Latitude": 104.4141114
 },
 {
   "STT": 70,
   "Name": "Trạm y tế xã Quang Minh",
   "address": "Xã Quang Minh - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.9926667,
   "Latitude": 104.6238048
 },
 {
   "STT": 71,
   "Name": "Trạm y tế xã Phong Dụ Hạ",
   "address": "Xã Phong Dô Hạ - Huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.8789076,
   "Latitude": 104.6908517
 },
 {
   "STT": 72,
   "Name": "Trạm y tế xã Xuân Tầm",
   "address": "Xã Xuân Tầm - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.8867122,
   "Latitude": 104.5072722
 },
 {
   "STT": 73,
   "Name": "Trạm y tế xã Hoàng Thắng",
   "address": "Xã Hoàng Thắng - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.7905575,
   "Latitude": 104.7345937
 },
 {
   "STT": 74,
   "Name": "Trạm y tế xã An Bình",
   "address": "Xã An Bình - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 22.0201729,
   "Latitude": 104.577181
 },
 {
   "STT": 75,
   "Name": "Trạm y tế xã Phong Dụ Thượng",
   "address": "Xã Phong Dô Thượng - huyện Văn Yên - tỉnh Yên Bái",
   "Longtitude": 21.9253045,
   "Latitude": 104.6631432
 },
 {
   "STT": 76,
   "Name": "Trạm y tế xã Bảo ái",
   "address": "Xã Bảo ái - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.9036983,
   "Latitude": 104.8337879
 },
 {
   "STT": 77,
   "Name": "Trạm y tế xã Tân Nguyên",
   "address": "Xã Tân Nguyên - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.931244,
   "Latitude": 104.7871004
 },
 {
   "STT": 78,
   "Name": "Trạm y tế xã Phúc An",
   "address": "Xã Phúc An - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.8463054,
   "Latitude": 105.0148322
 },
 {
   "STT": 79,
   "Name": "Trạm y tế xã Xuân Long",
   "address": "Xã Xuân Long - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 22.0135792,
   "Latitude": 104.9038451
 },
 {
   "STT": 80,
   "Name": "Trạm y tế xã Bạch Hà",
   "address": "Xã Bạch Hà - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.8123603,
   "Latitude": 105.0674317
 },
 {
   "STT": 81,
   "Name": "Trạm y tế xã Đại Minh",
   "address": "Xã Đại Minh - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.6934707,
   "Latitude": 105.0645091
 },
 {
   "STT": 82,
   "Name": "Trạm y tế xã Ngọc Chấn",
   "address": "Xã Ngọc Chên - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.9648937,
   "Latitude": 104.9272044
 },
 {
   "STT": 83,
   "Name": "Trạm y tế xã Tích Cốc",
   "address": "Xã Tích Cốc - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 22.0239819,
   "Latitude": 104.9797753
 },
 {
   "STT": 84,
   "Name": "Trạm y tế xã Phúc Ninh",
   "address": "Xã Phúc Ninh - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.9385111,
   "Latitude": 104.8980058
 },
 {
   "STT": 85,
   "Name": "Trạm y tế xã Mỹ Gia",
   "address": "Xã Mỹ Gia - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.9341969,
   "Latitude": 104.9447261
 },
 {
   "STT": 86,
   "Name": "Trạm y tế xã Xuân Lai",
   "address": "Xã Xuân Lai - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.9193226,
   "Latitude": 104.9797753
 },
 {
   "STT": 87,
   "Name": "Trạm y tế xã Tân Hương",
   "address": "Xã Tân Hương - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.8296141,
   "Latitude": 104.8804893
 },
 {
   "STT": 88,
   "Name": "Trạm y tế xã Mông Sơn",
   "address": "Xã Mông Sơn - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.8739976,
   "Latitude": 104.9038451
 },
 {
   "STT": 89,
   "Name": "Trạm y tế xã Yên Thành",
   "address": "Xã Yên Thành - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.8886271,
   "Latitude": 104.9973028
 },
 {
   "STT": 90,
   "Name": "Trạm y tế xã Đại Đồng",
   "address": "Xã Đại Đồng - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.7830038,
   "Latitude": 104.9447261
 },
 {
   "STT": 91,
   "Name": "Trạm y tế xã  Phú Thịnh",
   "address": "Xã Phú Thịnh - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.7110775,
   "Latitude": 104.9680914
 },
 {
   "STT": 92,
   "Name": "Trạm y tế xã Vũ Linh",
   "address": "Xã Vũ Linh - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.8120584,
   "Latitude": 105.0278389
 },
 {
   "STT": 93,
   "Name": "Trạm y tế xã Vĩnh Kiên",
   "address": "Xã Vĩnh Kiên - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.7701868,
   "Latitude": 105.0206757
 },
 {
   "STT": 94,
   "Name": "Trạm y tế xã Yên Bình",
   "address": "Xã Yên Bình - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.7621418,
   "Latitude": 105.0628768
 },
 {
   "STT": 95,
   "Name": "Trạm y tế xã Hán Đà",
   "address": "Xã Hán Đà - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.7267924,
   "Latitude": 105.0498966
 },
 {
   "STT": 96,
   "Name": "Trạm y tế xã Thịnh Hưng",
   "address": "Xã Thịnh Hưng - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.7078528,
   "Latitude": 105.0031457
 },
 {
   "STT": 97,
   "Name": "Trạm y tế xã Púng Luông",
   "address": "Xã Póng Luông - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.7670046,
   "Latitude": 104.1466046
 },
 {
   "STT": 98,
   "Name": "Trạm y tế xã Nậm Có",
   "address": "Xã Nậm Cã - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.8693404,
   "Latitude": 104.3093759
 },
 {
   "STT": 99,
   "Name": "Trạm y tế xã Cao Phạ",
   "address": "Xã Cao Phạ - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.7928603,
   "Latitude": 104.2512221
 },
 {
   "STT": 100,
   "Name": "Trạm y tế xã Nậm Khắt",
   "address": "Xã Nậm Khắt - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.7016339,
   "Latitude": 104.2279672
 },
 {
   "STT": 101,
   "Name": "Trạm y tế xã Chế Tạo",
   "address": "Xã Chõ Tạo - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.7070562,
   "Latitude": 104.0304535
 },
 {
   "STT": 102,
   "Name": "Trạm y tế xã Hồ Bốn",
   "address": "Xã Hồ Bèn - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.8910241,
   "Latitude": 103.9260001
 },
 {
   "STT": 103,
   "Name": "Trạm y tế xã Khao Mang",
   "address": "Xã Khao Mang - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.8752865,
   "Latitude": 103.9818438
 },
 {
   "STT": 104,
   "Name": "Trạm y tế xã Dế Xu Phình",
   "address": "Xã Dõ Su Phình - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.7796739,
   "Latitude": 104.1349852
 },
 {
   "STT": 105,
   "Name": "Trạm y tế xã Lao Chải",
   "address": "Xã Lao Chải - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.80437,
   "Latitude": 103.98402
 },
 {
   "STT": 106,
   "Name": "Trạm y tế xã La Pán Tẩn",
   "address": "Xã La Pán Tèn - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.7936291,
   "Latitude": 104.1756573
 },
 {
   "STT": 107,
   "Name": "Trạm y tế xã Chế Cu Nha",
   "address": "Xã Chõ Cu Nha - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.868909,
   "Latitude": 104.1814686
 },
 {
   "STT": 108,
   "Name": "Trạm y tế xã Kim Nọi",
   "address": "Xã Kim Nọi - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.8250221,
   "Latitude": 104.0827096
 },
 {
   "STT": 109,
   "Name": "Trạm y tế xã Mồ Dề",
   "address": "Xã Mồ D_ - huyện Mù Căng Chải - tỉnh Yên Bái",
   "Longtitude": 21.8749841,
   "Latitude": 104.1117492
 },
 {
   "STT": 110,
   "Name": "Trạm y tế xã Phù Nham",
   "address": "Xã Phù Nham - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.6231036,
   "Latitude": 104.5247463
 },
 {
   "STT": 111,
   "Name": "Trạm y tế xã Sơn A",
   "address": "Xã Sơn A - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.6341632,
   "Latitude": 104.4985359
 },
 {
   "STT": 112,
   "Name": "Trạm y tế TT NT Trần phú",
   "address": "Thị Trấn Nt Trần Phú - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.4615524,
   "Latitude": 104.7695962
 },
 {
   "STT": 113,
   "Name": "Trạm y tế xã Nậm Búng",
   "address": "Xã Nậm Bóng - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.4298431,
   "Latitude": 104.798771
 },
 {
   "STT": 114,
   "Name": "Trạm y tế xã Đại Lịch",
   "address": "Xã Đại Lịch - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.5335065,
   "Latitude": 104.8104424
 },
 {
   "STT": 115,
   "Name": "Trạm y tế xã Bình Thuận",
   "address": "Xã Bình Thuận - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.4341122,
   "Latitude": 104.8804893
 },
 {
   "STT": 116,
   "Name": "Trạm y tế xã Suối Giàng",
   "address": "Xã Suối Giàng - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.6221853,
   "Latitude": 104.6004911
 },
 {
   "STT": 117,
   "Name": "Trạm y tế xã Minh An",
   "address": "Xã Minh An - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.3685259,
   "Latitude": 104.8337879
 },
 {
   "STT": 118,
   "Name": "Trạm y tế xã Nghĩa Tâm",
   "address": "Xã Nghĩa Tâm - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.4150596,
   "Latitude": 104.8337879
 },
 {
   "STT": 119,
   "Name": "Trạm y tế xã Thượng Bằng La",
   "address": "Xã Thượng Bằng La - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.3980763,
   "Latitude": 104.7637619
 },
 {
   "STT": 120,
   "Name": "Trạm y tế xã Tú Lệ",
   "address": "Xã Tú Lệ - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.7899883,
   "Latitude": 104.2966426
 },
 {
   "STT": 121,
   "Name": "Trạm y tế xã Phúc Sơn",
   "address": "Xã Phúc Sơn - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.5446695,
   "Latitude": 104.4898001
 },
 {
   "STT": 122,
   "Name": "Trạm y tế xã Thanh Lương",
   "address": "Xã Thanh Lương - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.5680425,
   "Latitude": 104.5218338
 },
 {
   "STT": 123,
   "Name": "Trạm y tế xã Sơn Thịnh",
   "address": "Xã Sơn Thịnh - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.5703097,
   "Latitude": 104.5946633
 },
 {
   "STT": 124,
   "Name": "Trạm y tế TT Nông trường Liên Sơn",
   "address": "Thị trấn NT Liên Sơn - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.6526665,
   "Latitude": 104.4868883
 },
 {
   "STT": 125,
   "Name": "Trạm y tế xã Nghĩa Sơn",
   "address": "Xã Nghĩa Sơn - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.6049654,
   "Latitude": 104.4665072
 },
 {
   "STT": 126,
   "Name": "Trạm y tế xã Nậm Lành",
   "address": "Xã Nậm Lành - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.6599568,
   "Latitude": 104.4373961
 },
 {
   "STT": 127,
   "Name": "Trạm y tế xã An Lương",
   "address": "Xã An Lương - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.717418,
   "Latitude": 104.577181
 },
 {
   "STT": 128,
   "Name": "Trạm y tế xã Thạch Lương",
   "address": "Xã Thạch Lương - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.5394424,
   "Latitude": 104.516009
 },
 {
   "STT": 129,
   "Name": "Trạm y tế xã Hạnh Sơn",
   "address": "Xã Hạnh Sơn - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.5706187,
   "Latitude": 104.492712
 },
 {
   "STT": 130,
   "Name": "Trạm y tế xã Nậm Mười",
   "address": "Xã Nậm Mười - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.7257237,
   "Latitude": 104.4839765
 },
 {
   "STT": 131,
   "Name": "Trạm y tế xã Sùng Đô",
   "address": "Xã Sùng Đô - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.7215781,
   "Latitude": 104.5305715
 },
 {
   "STT": 132,
   "Name": "Trạm y tế xã Suối Quyền",
   "address": "Xã Suối Quyền - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.6559699,
   "Latitude": 104.5480484
 },
 {
   "STT": 133,
   "Name": "Trạm y tế xã Suối Bu",
   "address": "Xã Suối Bu - Huyện Văn Chấn - Tỉnh Yên Bái",
   "Longtitude": 21.5651104,
   "Latitude": 104.652952
 },
 {
   "STT": 134,
   "Name": "Trạm y tế Báo Đáp",
   "address": "Xã Báo Đáp - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.8222919,
   "Latitude": 104.7695962
 },
 {
   "STT": 135,
   "Name": "Trạm y tế xã Y Can",
   "address": "Xã Y Can - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.7165007,
   "Latitude": 104.7812654
 },
 {
   "STT": 136,
   "Name": "Trạm y tế xã Văn Tiến",
   "address": "TP Yên Bái",
   "Longtitude": 21.7167689,
   "Latitude": 104.8985878
 },
 {
   "STT": 137,
   "Name": "Trạm y tế xã Giới Phiên",
   "address": "TP Yên Bái",
   "Longtitude": 21.7167689,
   "Latitude": 104.8985878
 },
 {
   "STT": 138,
   "Name": "Trạm y tế xã Bảo Hưng",
   "address": "Xã Bảo Hưng - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.6720404,
   "Latitude": 104.8863279
 },
 {
   "STT": 139,
   "Name": "Trạm y tế xã Tân Đồng",
   "address": "Xã Tân Đồng - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.8550753,
   "Latitude": 104.7929356
 },
 {
   "STT": 140,
   "Name": "Trạm y tế xã Lương Thịnh",
   "address": "Xã Lương Thịnh - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.6519722,
   "Latitude": 104.7871004
 },
 {
   "STT": 141,
   "Name": "Trạm y tế xã Quy Mông",
   "address": "Xã Quy Mông - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.7746834,
   "Latitude": 104.7812654
 },
 {
   "STT": 142,
   "Name": "Trạm y tế xã Đào Thịnh",
   "address": "Xã Đào Thịnh - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.8191029,
   "Latitude": 104.8046066
 },
 {
   "STT": 143,
   "Name": "Trạm y tế xã Việt thành",
   "address": "Xã Việt Thành - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.7921248,
   "Latitude": 104.8133604
 },
 {
   "STT": 144,
   "Name": "Trạm y tế xã Hoà Cuông",
   "address": "Xã Hòa Cuông - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.814838,
   "Latitude": 104.8512993
 },
 {
   "STT": 145,
   "Name": "Trạm y tế xã Kiên Thành",
   "address": "Xã Kiên Thành - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.7291641,
   "Latitude": 104.7523821
 },
 {
   "STT": 146,
   "Name": "Trạm y tế Xã Minh Tiến",
   "address": "Xã Minh Tiến - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.7238942,
   "Latitude": 104.8279512
 },
 {
   "STT": 147,
   "Name": "Trạm y tế xã Nga Quán",
   "address": "Xã Nga Quán - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.7487421,
   "Latitude": 104.8425433
 },
 {
   "STT": 148,
   "Name": "Trạm y tế xã Minh Quân",
   "address": "Xã Minh Quân - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.6487758,
   "Latitude": 104.8863279
 },
 {
   "STT": 149,
   "Name": "Trạm y tế xã Cường Thịnh",
   "address": "Xã Cường Thịnh - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.7545353,
   "Latitude": 104.8746508
 },
 {
   "STT": 150,
   "Name": "Trạm y tế xã Âu Lâu",
   "address": "Xã Âu Lâu - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.6160083,
   "Latitude": 104.798771
 },
 {
   "STT": 151,
   "Name": "Trạm y tế xã Hợp Minh",
   "address": "Xã Hợp Minh - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.693344,
   "Latitude": 104.867577
 },
 {
   "STT": 152,
   "Name": "Trạm y tế xã Hồng Ca",
   "address": "Xã Hồng Ca - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.5905563,
   "Latitude": 104.6937674
 },
 {
   "STT": 153,
   "Name": "TRạm y tế xã Hưng Thịnh",
   "address": "Xã Hưng Thịnh - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.5883893,
   "Latitude": 104.7832847
 },
 {
   "STT": 154,
   "Name": "Trạm y tế xã Minh Quán",
   "address": "Xã Minh Quán - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.6487758,
   "Latitude": 104.8863279
 },
 {
   "STT": 155,
   "Name": "Trạm y tế xã Phúc Lộc",
   "address": "Xã Phúc Lộc - TP Yên Bái",
   "Longtitude": 21.7167689,
   "Latitude": 104.8985878
 },
 {
   "STT": 156,
   "Name": "Trạm y tế xã Văn Phú",
   "address": "Xã Văn Phú - TP Yên Bái",
   "Longtitude": 21.6794024,
   "Latitude": 104.9330448
 },
 {
   "STT": 157,
   "Name": "Trạm y tế xã Văn Lãng",
   "address": "Xã Văn Lãng - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.6645609,
   "Latitude": 104.9680914
 },
 {
   "STT": 158,
   "Name": "Trạm y tế xã Việt Hồng",
   "address": "Xã Việt Hồng - huyện Trên Yên - tỉnh Yên Bái",
   "Longtitude": 21.5779337,
   "Latitude": 104.8337879
 },
 {
   "STT": 159,
   "Name": "Trạm y tế xã Vân Hội",
   "address": "Xã Vân Hội - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.5800437,
   "Latitude": 104.8746508
 },
 {
   "STT": 160,
   "Name": "Trạm y tế xã Trạm Tấu",
   "address": "Xã Trạm Tấu - Huyện Trạm Tấu - Tỉnh Yên Bái",
   "Longtitude": 21.5283185,
   "Latitude": 104.3442794
 },
 {
   "STT": 161,
   "Name": "Trạm y tế xã Hát Lừu",
   "address": "Xã Hát Lìu - Huyện Trạm Tấu - Tỉnh Yên Bái",
   "Longtitude": 21.436359,
   "Latitude": 104.3966503
 },
 {
   "STT": 162,
   "Name": "Trạm y tế xã Túc Đán",
   "address": "Xã Túc Đán - Huyện Trạm Tấu - Tỉnh Yên Bái",
   "Longtitude": 21.5814817,
   "Latitude": 104.4024704
 },
 {
   "STT": 163,
   "Name": "Trạm y tế xã Xà Hồ",
   "address": "Xã Xà Hồ - Huyện Trạm Tấu - Tỉnh Yên Bái",
   "Longtitude": 21.5283185,
   "Latitude": 104.3442794
 },
 {
   "STT": 164,
   "Name": "Trạm y tế xã Pá Lau",
   "address": "Xã Pá Lau - Huyện Trạm Tấu - Tỉnh Yên Bái",
   "Longtitude": 21.5381493,
   "Latitude": 104.4315746
 },
 {
   "STT": 165,
   "Name": "Trạm y tế xã Bản Công",
   "address": "Xã Bản Công - Huyện Trạm Tấu - Tỉnh Yên Bái",
   "Longtitude": 21.4629385,
   "Latitude": 104.4257533
 },
 {
   "STT": 166,
   "Name": "Trạm y tế xã Pá Hu",
   "address": "Xã Pá Hu - Huyện Trạm Tấu - Tỉnh Yên Bái",
   "Longtitude": 21.4895071,
   "Latitude": 104.4548621
 },
 {
   "STT": 167,
   "Name": "Trạm y tế xã Làng Nhì",
   "address": "Xã Làng Nhì - Huyện Trạm Tấu - Tỉnh Yên Bái",
   "Longtitude": 21.5020278,
   "Latitude": 104.536689
 },
 {
   "STT": 168,
   "Name": "Trạm y tế xã Phình Hồ",
   "address": "Xã Phình Hồ - Huyện Trạm Tấu - Tỉnh Yên Bái",
   "Longtitude": 21.5277416,
   "Latitude": 104.5381524
 },
 {
   "STT": 169,
   "Name": "Trạm y tế xã Bản Mù",
   "address": "Xã Bản Mù - Huyện Trạm Tấu - Tỉnh Yên Bái",
   "Longtitude": 21.3910067,
   "Latitude": 104.4490398
 },
 {
   "STT": 170,
   "Name": "Trạm y tế xã Tà Si Láng",
   "address": "Xã Tà Si Láng - Huyện Trạm Tấu - Tỉnh Yên Bái",
   "Longtitude": 21.4824683,
   "Latitude": 104.6004911
 },
 {
   "STT": 171,
   "Name": "Trạm y tế xã Nghĩa Phúc",
   "address": "Xã Nghĩa Phúc - Thị xã Nghĩa Lộ - Tỉnh Yên Bái",
   "Longtitude": 21.6235473,
   "Latitude": 104.4868883
 },
 {
   "STT": 172,
   "Name": "Trạm y tế xã Nghĩa Lợi",
   "address": "Xã Nghĩa Lợi - Thị xã Nghĩa Lộ - Tỉnh Yên Bái",
   "Longtitude": 21.6029811,
   "Latitude": 104.5218338
 },
 {
   "STT": 173,
   "Name": "Trạm y tế xã Nghĩa An",
   "address": "Xã Nghĩa An - Thị xã Nghĩa Lộ - Tỉnh Yên Bái",
   "Longtitude": 21.6018769,
   "Latitude": 104.5062651
 }
];