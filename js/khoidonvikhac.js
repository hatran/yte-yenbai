var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Trung tâm phòng chống bệnh xã hội tỉnh Yên Bái",
   "area": "Tỉnh",
   "address": "Phường Minh Tân - TP Yên Bái - Tỉnh Yên Bái",
   "icon": "maps/map-images/hiv.png",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 2,
   "Name": "Trung tâm chăm sóc sức khỏe sinh sản tỉnh Yên Bái",
   "area": "Tỉnh",
   "address": "Phường Minh Tân - TP Yên Bái - Tỉnh Yên Bái",
   "icon": "maps/map-images/skss.png",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 3,
   "Name": "Ban Bảo vệ chăm sóc sức khỏe cán bộ",
   "icon": "maps/map-images/ttkn.png",
   "area": "Tỉnh",
   "address": "Số 721A, đường Yên Ninh , phường Minh Tân,  thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7263889,
   "Latitude": 104.8933333
 },
 {
   "STT": 4,
   "Name": "Bệnh xá công an tỉnh Yên Bái",
   "icon": "maps/map-images/placeholder-xs1.png",
   "area": "Huyện",
   "address": "Tổ 42 - phường Yên Ninh - Thành phố Yên Bái - Tỉnh Yên Bái",
   "Longtitude": 21.726217,
   "Latitude": 104.9064916
 },
 {
   "STT": 5,
   "Name": "Bệnh xá Chè Việt Cường",
   "icon": "maps/map-images/placeholder-xs1.png",
   "area": "Xã",
   "address": "Xã Việt Cường",
   "Longtitude": 21.6223504,
   "Latitude": 104.8571368
 },
 {
   "STT": 6,
   "Name": "Bệnh xá lâm trường Lục Yên",
   "icon": "maps/map-images/placeholder-xs1.png",
   "area": "Xã",
   "address": "Huyện Lục Yên",
   "Longtitude": 22.0900049,
   "Latitude": 104.705431
 },
 {
   "STT": 7,
   "Name": "Bệnh xá Lâm trường Văn Yên",
   "icon": "maps/map-images/placeholder-xs1.png",
   "area": "Huyện",
   "address": "Huyện Văn Yên",
   "Longtitude": 21.8698428,
   "Latitude": 104.5655273
 },
 {
   "STT": 8,
   "Name": "Bệnh xá công ty chè Văn Hưng",
   "icon": "maps/map-images/placeholder-xs1.png",
   "area": "Huyện",
   "address": "Huyện Yên Bình",
   "Longtitude": 21.7605878,
   "Latitude": 105.0615865
 },
 {
   "STT": 9,
   "Name": "Phòng Y tế công ty quản lý đường sắt Yên Lào",
   "icon": "maps/map-images/placeholder-xs1.png",
   "area": "Huyện",
   "address": "Phường Hồng Hà - TP Yên Bái",
   "Longtitude": 21.6976918,
   "Latitude": 104.8761104
 }
];