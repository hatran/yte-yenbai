var datanhathuoc = [
 {
   "STT": 1,
   "Name": "Quầy thuốc Nguyễn Thị Kim Oanh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Km 12, tổ 11, thị trấn Yên Bình, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.7459652,
   "Latitude": 104.9680914
 },
 {
   "STT": 2,
   "Name": "Quầy thuốc Ngô Đức Quân, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Chợ Km 19, xã Cẩm Ân, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.8533186,
   "Latitude": 104.8615569
 },
 {
   "STT": 3,
   "Name": "Quầy thuốc Đỗ Thị Hồng Điệp, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Khu I, thị trấn Thác Bà, huyện Yên Bình, tỉnh Yên Bái ",
   "Longtitude": 21.743216,
   "Latitude": 105.0294415
 },
 {
   "STT": 4,
   "Name": "Quầy thuốc Hoàng Thị Lụa, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Phạ 1, xã Cẩm Nhân, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.9761737,
   "Latitude": 104.972062
 },
 {
   "STT": 5,
   "Name": "Quầy thuốc Như Quỳnh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 5, xã Thịnh Hưng, huyện Yên Bình, tỉnh Yên Bái.",
   "Longtitude": 21.6934805,
   "Latitude": 104.9961662
 },
 {
   "STT": 6,
   "Name": "Quầy thuốc Nguyễn Thị Hồng Hạnh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Thác Ông, xã Vĩnh Kiên, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.7620506,
   "Latitude": 105.0529861
 },
 {
   "STT": 7,
   "Name": "Quầy thuốc Vũ Thị Thanh Minh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Km 13, tổ 14A, thị trấn Yên Bình, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.725104,
   "Latitude": 104.96652
 },
 {
   "STT": 8,
   "Name": "Quầy thuốc Đinh Thị Vân, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Km 32, xã Tân Nguyên, huyện Yên Bình, tỉnh Yên Bái.",
   "Longtitude": 21.931244,
   "Latitude": 104.7871004
 },
 {
   "STT": 9,
   "Name": "Quầy thuốc Huyền Trang, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Đoàn Kết, xã Cẩm Ân, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.8603032,
   "Latitude": 104.8629746
 },
 {
   "STT": 10,
   "Name": "Quầy thuốc Đỗ Thị Thúy Nga, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Đồng Tâm, xã Phúc An, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.7190744,
   "Latitude": 104.9126044
 },
 {
   "STT": 11,
   "Name": "Quầy thuốc Đinh Thị Nguyệt, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 3, Xã Xuân Long, huyện Yên Bình",
   "Longtitude": 22.0202347,
   "Latitude": 104.8790296
 },
 {
   "STT": 12,
   "Name": "Quầy thuốc Vũ Thị Bích Hồng, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 10, xã Cẩm Nhân, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.9715666,
   "Latitude": 104.9724305
 },
 {
   "STT": 13,
   "Name": "Quầy thuốc Nông Thị Phượng, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 5, xã Xuân Long, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 22.0202347,
   "Latitude": 104.8790296
 },
 {
   "STT": 14,
   "Name": "Quầy thuốc Minh Nguyệt, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 5, xã Thịnh Hưng, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.6934805,
   "Latitude": 104.9961662
 },
 {
   "STT": 15,
   "Name": "Quầy thuốc Hải Yến, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Thác Ông, Xã Vĩnh Kiên, H. Yên Bình",
   "Longtitude": 21.7620506,
   "Latitude": 105.0529861
 },
 {
   "STT": 16,
   "Name": "Quầy thuốc Hoàng Thị Bảy, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Phúc Hòa 2, xã Hán Đà, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.7104215,
   "Latitude": 105.063935
 },
 {
   "STT": 17,
   "Name": "Quầy thuốc Thành Phương, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Km 23, thôn Ngòi Khang, xã Bảo Ái, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.9036983,
   "Latitude": 104.8337879
 },
 {
   "STT": 18,
   "Name": "Quầy thuốc Tiền Phong, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Trung Tâm, xã Yên Bình, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.7621418,
   "Latitude": 105.0628768
 },
 {
   "STT": 19,
   "Name": "Quầy thuốc Quỳnh Anh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 3, xã Đại Đồng, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.7882399,
   "Latitude": 105.0027837
 },
 {
   "STT": 20,
   "Name": "Quầy thuốc Đoàn Thị Vịnh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Trung Tâm, xã Xuân Lai, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.9145558,
   "Latitude": 104.984157
 },
 {
   "STT": 21,
   "Name": "Quầy thuốc Trần Thị Kim Huế, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Đồng Tâm, xã Phúc An, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.7190744,
   "Latitude": 104.9126044
 },
 {
   "STT": 22,
   "Name": "Quầy thuốc Dũng Hằng, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Tích Chung II, xã Cảm Nhân, huyện Yên Bình, tỉnh Yên Bái. ",
   "Longtitude": 21.9715666,
   "Latitude": 104.9724305
 },
 {
   "STT": 23,
   "Name": "Quầy thuốc Vi Thị Dịu, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Khe Hùm, xã Tân Nguyên, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.931244,
   "Latitude": 104.7871004
 },
 {
   "STT": 24,
   "Name": "Quầy thuốc số 6, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Km 14, Tổ 14B, thị trấn Yên Bình, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.7459652,
   "Latitude": 104.9680914
 },
 {
   "STT": 25,
   "Name": "Quầy thuốc Trương Thị Chinh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 8, xã Khánh Hòa, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.1190667,
   "Latitude": 104.6546811
 },
 {
   "STT": 26,
   "Name": "Quầy thuốc Nguyễn Thị Tuyến, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Trung Tâm, xã Tân Lĩnh, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.0888435,
   "Latitude": 104.7072517
 },
 {
   "STT": 27,
   "Name": "Quầy thuốc Hoàng Văn Dùng, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Ki ốt chợ Mường Lai, xã Mường Lai, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.1596549,
   "Latitude": 104.8337879
 },
 {
   "STT": 28,
   "Name": "Quầy thuốc Vũ Thị Tươi, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 17, thị trấn Yên Thế, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.110575,
   "Latitude": 104.766679
 },
 {
   "STT": 29,
   "Name": "Quầy thuốc Hoàng Bích Hưởng, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 10, xã Minh Xuân, huyện Lục Yên, tỉnh Yên Bái.",
   "Longtitude": 22.1406957,
   "Latitude": 104.7871004
 },
 {
   "STT": 30,
   "Name": "Quầy thuốc Trung Tâm Số 1B, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 10, thị trấn Yên Thế, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.1137557,
   "Latitude": 104.7687389
 },
 {
   "STT": 31,
   "Name": "Quầy thuốc Trung Tâm Số 1A, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 10, thị trấn Yên Thế, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.1137557,
   "Latitude": 104.7687389
 },
 {
   "STT": 32,
   "Name": "Quầy thuốc Yến Duyên, Công ty Cổ phần Dược phẩm Yên Bái",
   "address": "Số 398, đường Nguyễn Tất Thành, tổ 10, thị trấn Yên Thế, huyện Lục Yên, tỉnh Yên Bái.",
   "Longtitude": 22.0899425,
   "Latitude": 104.7695962
 },
 {
   "STT": 33,
   "Name": "Quầy thuốc Hoàng Thị Loan, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 6, xã Động Quan, huyện Lục Yên, tỉnh Yên Bái ",
   "Longtitude": 22.0900049,
   "Latitude": 104.705431
 },
 {
   "STT": 34,
   "Name": "Quầy thuốc Hoàng Thị Thế, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Trung Tâm, xã Tân Lĩnh, huyện Lục Yên, tỉnh Yên Bái ",
   "Longtitude": 22.0888435,
   "Latitude": 104.7072517
 },
 {
   "STT": 35,
   "Name": "Quầy thuốc Hoàng Văn Hùng, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 10, thị trấn Yên Thế, huyện  Lục Yên, tỉnh Yên Bái.",
   "Longtitude": 22.1137557,
   "Latitude": 104.7687389
 },
 {
   "STT": 36,
   "Name": "Quầy thuốc Hoàng Thị Đến, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 9, xã Minh Xuân, huyện Lục Yên, tỉnh Yên Bái.",
   "Longtitude": 22.1406957,
   "Latitude": 104.7871004
 },
 {
   "STT": 37,
   "Name": "Quầy thuốc Nông Thanh Hồng, Công ty cổ phần DP Yên Bái ",
   "address": "Tổ 13, thị trấn Yên Thế, huyện Lục Yên, tỉnh Yên Bái ",
   "Longtitude": 22.110575,
   "Latitude": 104.766679
 },
 {
   "STT": 38,
   "Name": "Quầy thuốc Nông Thị Thu Hường, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Sơn Trung, xã Mai Sơn, huyện Lục Yên, tỉnh Yên Bái.",
   "Longtitude": 22.1873306,
   "Latitude": 104.7229279
 },
 {
   "STT": 39,
   "Name": "Quầy thuốc Trịnh Thị Kim Ánh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Khu Trung Tâm, xã Tân Lĩnh, huyện Lục Yên, tỉnh Yên Bái.",
   "Longtitude": 22.123854,
   "Latitude": 104.7170954
 },
 {
   "STT": 40,
   "Name": "Quầy thuốc Lương Thị Lĩnh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Ngã tư bờ hồ, thị trấn Yên Thế, huyện Lục Yên, tỉnh Yên Bái.",
   "Longtitude": 22.1127766,
   "Latitude": 104.7685182
 },
 {
   "STT": 41,
   "Name": "Quầy thuốc Hoàng Thị Thuyên, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Nà Pồng, xã Lâm Thượng, huyện Lục Yên",
   "Longtitude": 22.2212452,
   "Latitude": 104.670443
 },
 {
   "STT": 42,
   "Name": "Quầy thuốc Hà Thị Dung, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Sơn Trung, xã Mai Sơn, huyện Lục Yên",
   "Longtitude": 22.1873306,
   "Latitude": 104.7229279
 },
 {
   "STT": 43,
   "Name": "Quầy thuốc Nguyễn Thị Phú, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 10, thị trấn  Yên Thế, huyện Lục Yên, tỉnh Yên Bái ",
   "Longtitude": 22.1137557,
   "Latitude": 104.7687389
 },
 {
   "STT": 44,
   "Name": "Quầy thuốc Hoàng Thị Vẽ, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 17, thị trấn Yên Thế, huyện Lục Yên, tỉnh Yên Bái.",
   "Longtitude": 22.110575,
   "Latitude": 104.766679
 },
 {
   "STT": 45,
   "Name": "Quầy thuốc Nông Thị Nhịp, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Bản Hin Lạn B, xã Lâm Thượng, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.2212452,
   "Latitude": 104.670443
 },
 {
   "STT": 46,
   "Name": "Quầy thuốc Hoàng Thị Tia, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Bản Chỏi, xã Lâm Thượng, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.1819065,
   "Latitude": 104.6997717
 },
 {
   "STT": 47,
   "Name": "Quầy thuốc số D14 Nguyễn Thị Hồng Nhung, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 16, xã Động Quan, huyện Lục Yên, tỉnh Yên Bái.",
   "Longtitude": 22.095731,
   "Latitude": 104.715044
 },
 {
   "STT": 48,
   "Name": "Quầy thuốc Hoàng Văn Vũ, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Nà Khao, xã Yên Thắng, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.1269975,
   "Latitude": 104.7462603
 },
 {
   "STT": 49,
   "Name": "Quầy thuốc Hoàng Thị Lấy, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Tông Áng, xã Khánh Thiện, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.235204,
   "Latitude": 104.7243861
 },
 {
   "STT": 50,
   "Name": "Quầy thuốc Hà Thị Thu Hiếu, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 2, Xã Minh Chuẩn, huyện Lục Yên, Tỉnh Yên Bái",
   "Longtitude": 22.1831695,
   "Latitude": 104.6412925
 },
 {
   "STT": 51,
   "Name": "Quầy thuốc Phí Thị Tuyết, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 6, thị trấn Yên Thế, huyện Lục Yên, tỉnh Yên Bái ",
   "Longtitude": 22.1131196,
   "Latitude": 104.7673656
 },
 {
   "STT": 52,
   "Name": "Quầy thuốc Trần Thị Thế , Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Sơn Trung, xã Mai Sơn, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.1873306,
   "Latitude": 104.7229279
 },
 {
   "STT": 53,
   "Name": "Quầy thuốc Nông Thị Xuân, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 2, xã Phúc Lợi, huyện Lục Yên, tỉnh Yên Bái ",
   "Longtitude": 22.0152912,
   "Latitude": 104.708935
 },
 {
   "STT": 54,
   "Name": "Quầy thuốc Lê Thị Thu, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Tân Quang, xã Liễu Đô, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.0867224,
   "Latitude": 104.8046066
 },
 {
   "STT": 55,
   "Name": "Quầy thuốc Mai Tuấn Long, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 10, thị trấn Yên Thế, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.1137557,
   "Latitude": 104.7687389
 },
 {
   "STT": 56,
   "Name": "Quầy thuốc Nguyễn Thị Diệp, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Cây Mơ, xã Liễu Đô, huyện Lục Yên, tỉnh Yên Bái ",
   "Longtitude": 22.0900049,
   "Latitude": 104.705431
 },
 {
   "STT": 57,
   "Name": "Quầy thuốc Hoàng Thị Hương Giang, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Hồng Sơn, Xã Sơn Thịnh, huyện Văn Chấn, Tỉnh Yên Bái",
   "Longtitude": 21.4298431,
   "Latitude": 104.798771
 },
 {
   "STT": 58,
   "Name": "Quầy thuốc  số 27 -Công ty cổ phần dược phẩm Yên Bái",
   "address": "Khu 2, Ngã Ba xã Cát Thịnh, huyện Văn Chấn, tỉnh Yên Bái ",
   "Longtitude": 21.4508836,
   "Latitude": 104.6937674
 },
 {
   "STT": 59,
   "Name": "Quầy thuốc Lê Thị Ly, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 13, xã Tân Thịnh, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.498047,
   "Latitude": 104.7681376
 },
 {
   "STT": 60,
   "Name": "Quầy thuốc Phạm Thị Thu, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 4, xã Đại Lịch, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.537872,
   "Latitude": 104.818467
 },
 {
   "STT": 61,
   "Name": "Quầy thuốc Hà Thị Hiền, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 14, xã Tân Thịnh, huyện Văn Chấn, tỉnh Yên Bái.",
   "Longtitude": 21.5080977,
   "Latitude": 104.7695962
 },
 {
   "STT": 62,
   "Name": "Quầy thuốc Duy Hiếu, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ dân phố 10A, thị trấn Nông trường Trần Phú, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.4610169,
   "Latitude": 104.7711563
 },
 {
   "STT": 63,
   "Name": "Quầy thuốc Bình Liên, Công ty cổ phần Dược phẩm Yên Bái  ",
   "address": "Tổ 2, Thị trấn Nông trường Liên Sơn, huyện Văn Chấn, tỉnh Yên Bái ",
   "Longtitude": 21.6526665,
   "Latitude": 104.4868883
 },
 {
   "STT": 64,
   "Name": "Quầy thuốc Trương Thị Mùi, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Trung Tâm, xã Nậm Búng, huyện Văn Chấn, tỉnh Yên Bái.",
   "Longtitude": 21.7253962,
   "Latitude": 104.3559158
 },
 {
   "STT": 65,
   "Name": "Quầy thuốc Trần Thị Cúc, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 14, xã Tân Thịnh, huyện  Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.5080977,
   "Latitude": 104.7695962
 },
 {
   "STT": 66,
   "Name": "Quầy thuốc Đào Thu Hương, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Pom Ban, Xã Tú Lệ, huyện Văn Chấn",
   "Longtitude": 21.7899883,
   "Latitude": 104.2966426
 },
 {
   "STT": 67,
   "Name": "Quầy thuốc Lê Thị Mạnh, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Tổ 8, thị trấn Nông trường Liên Sơn, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.6653829,
   "Latitude": 104.491256
 },
 {
   "STT": 68,
   "Name": "Quầy thuốc Đào Thanh Hà, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Tổ 7, thị trấn Nông Trường Nghĩa Lộ, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.5664941,
   "Latitude": 104.5393097
 },
 {
   "STT": 69,
   "Name": "Quầy thuốc Dương Thị Thu, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Thôn 9, xã Tân Thịnh, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.5080977,
   "Latitude": 104.7695962
 },
 {
   "STT": 70,
   "Name": "Quầy thuốc Trần Thị Thu Hằng, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Thôn Pom Pan, xã Tú Lệ, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.7899883,
   "Latitude": 104.2966426
 },
 {
   "STT": 71,
   "Name": "Quầy thuốc Phạm Thị Thu Hòa, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Chợ Chùa, Chấn Thịnh, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.4879676,
   "Latitude": 104.832584
 },
 {
   "STT": 72,
   "Name": "Quầy thuốc Đặng Thị Hồng Gấm, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Tổ 1, thị trấn nông trường Liên Sơn, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.6526665,
   "Latitude": 104.4868883
 },
 {
   "STT": 73,
   "Name": "Quầy thuốc Nguyễn Thu Hằng, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số 192, tổ 14, phường Trung Tâm, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 74,
   "Name": "Quầy thuốc Bùi Đức Thuấn, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số 161, tổ 14, phường Trung Tâm, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 75,
   "Name": "Quầy thuốc Đinh Thị Mới, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số  161, tổ 14, phường Trung Tâm, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 76,
   "Name": "Quầy thuốc Nguyễn Thanh Hải, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Tổ 14, phường Trung Tâm, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 77,
   "Name": "Quầy thuốc Bùi Thanh Tùng, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số 29, đường Thanh Niên, tổ 8, phường Trung Tâm, thị xã Nghĩa Lộ, tỉnh Yên Bái. ",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 78,
   "Name": "Quầy thuốc Hoàng Thị Bích Liên, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số 28, Tổ 5, Phường Cầu Thia, , thị xã Nghĩa Lộ, tỉnh Yên Bái, Tỉnh Yên Bái ",
   "Longtitude": 21.5873546,
   "Latitude": 104.5174652
 },
 {
   "STT": 79,
   "Name": "Quầy thuốc Nguyễn Thị  Nga, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số  81, tổ 4, phường Pú Trạng, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.599737,
   "Latitude": 104.492712
 },
 {
   "STT": 80,
   "Name": "Quầy thuốc Nguyễn Thị Mai Phương, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số 389, tổ 3, phường Pú Trạng, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.599737,
   "Latitude": 104.492712
 },
 {
   "STT": 81,
   "Name": "Quầy thuốc Trần Thị  Ngân, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số 49, tổ 3, phường Cầu Thia, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.5873546,
   "Latitude": 104.5174652
 },
 {
   "STT": 82,
   "Name": "Quầy thuốc Nguyễn Hữu Đại , Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số  226, đường Nguyễn Quang Bích, Tổ dân phố 13, phường Pú Trạng, , thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.599737,
   "Latitude": 104.492712
 },
 {
   "STT": 83,
   "Name": "Quầy thuốc Tống Thị Cúc, Công ty cổ phần Dược phẩm Yên Bái ",
   "address": "Số 83, Tổ 4, phường Cầu Thia, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.5873546,
   "Latitude": 104.5174652
 },
 {
   "STT": 84,
   "Name": "Quầy thuốc Hà Thị Thoa, Công ty cổ phần Dược phẩm Yên Bái ",
   "address": "Tổ 1, phường Pú Trạng, Thị xã Nghĩa Lộ, tỉnh Yên Bái.",
   "Longtitude": 21.599737,
   "Latitude": 104.492712
 },
 {
   "STT": 85,
   "Name": "Quầy thuốc Trần Trúc Vi, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ dân phố 1, Phường Pú Trạng, Thị xã Nghĩa Lộ, Tỉnh Yên Bái",
   "Longtitude": 21.6018769,
   "Latitude": 104.5062651
 },
 {
   "STT": 86,
   "Name": "Quầy thuốc số 23, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ dân phố số 5, thị trấn Cổ Phúc, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.7625049,
   "Latitude": 104.8191965
 },
 {
   "STT": 87,
   "Name": "Quầy thuốc Nguyễn Thị Thúy, Công ty Cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 10, xã Báo Đáp, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8222919,
   "Latitude": 104.7695962
 },
 {
   "STT": 88,
   "Name": "Quầy thuốc Hoàng Thị Vượng, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Khu phố 2, thị trấn  Cổ Phúc, huyện Trấn Yên, tỉnh Yên Bái ",
   "Longtitude": 21.7625049,
   "Latitude": 104.8191965
 },
 {
   "STT": 89,
   "Name": "Quầy thuốc Nguyễn Thị Trung Thủy, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 4, xã Hưng Khánh, huyện Trấn Yên, tỉnh Yên Bái ",
   "Longtitude": 21.5940435,
   "Latitude": 104.7639158
 },
 {
   "STT": 90,
   "Name": "Quầy thuốc số 21, Công ty Cổ phần Dược phẩm Yên Bái ",
   "address": "Khu phố 9, Thị trấn Cổ Phúc, huyện Trấn Yên, tỉnh Yên Bái ",
   "Longtitude": 21.7625049,
   "Latitude": 104.8191965
 },
 {
   "STT": 91,
   "Name": "Quầy thuốc số 22, Công ty Cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Yên Thịnh, xã Kiên Thành, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.7310428,
   "Latitude": 104.7331354
 },
 {
   "STT": 92,
   "Name": "Quầy thuốc Vương Thị Thùy Dung, Công ty Cổ phần Dược phẩm Yên Bái",
   "address": "Khu phố 5, thị Trấn Cổ Phúc, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.7556237,
   "Latitude": 104.8308695
 },
 {
   "STT": 93,
   "Name": "Quầy thuốc Nguyễn Thị Thu Hiền, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 8, xã Hưng Khánh, huyện Trấn Yên, tỉnh Yên Bái ",
   "Longtitude": 21.5965597,
   "Latitude": 104.7644912
 },
 {
   "STT": 94,
   "Name": "Quầy thuốc Thân Thị Kim Anh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Minh Phú, xã Vân Hội, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.594335,
   "Latitude": 104.8672485
 },
 {
   "STT": 95,
   "Name": "Quầy thuốc Nguyễn Tiến Công, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 2, Xã Đào Thịnh, huyện Trấn Yên",
   "Longtitude": 21.8191029,
   "Latitude": 104.8046066
 },
 {
   "STT": 96,
   "Name": "Quầy thuốc Hoàng Thị Bích Liên, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Bản Bến, Xã Việt Hồng, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.5779337,
   "Latitude": 104.8337879
 },
 {
   "STT": 97,
   "Name": "Quầy thuốc Triệu Thị Quyên, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Bản Nậm Khắt, xã Nậm Khắt, huyện Mù Cang Chải, tỉnh Yên Bái",
   "Longtitude": 21.7016339,
   "Latitude": 104.2279672
 },
 {
   "STT": 98,
   "Name": "Quầy thuốc Nguyễn Thị Hải, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Bản Thái, xã Khao Mang, huyện Mù Cang Chải, tỉnh Yên Bái",
   "Longtitude": 21.906364,
   "Latitude": 104.0188436
 },
 {
   "STT": 99,
   "Name": "Quầy thuốc Nguyễn Thị Hoa, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 3, thị trấn Mù Cang Chải, huyện Mù Cang Chải, tỉnh Yên Bái",
   "Longtitude": 21.8528313,
   "Latitude": 104.0836104
 },
 {
   "STT": 100,
   "Name": "Quầy thuốc số 36, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Khu II, thị trấn  Trạm Tấu, huyện Trạm Tấu, tỉnh Yên Bái",
   "Longtitude": 21.5148504,
   "Latitude": 104.4315746
 },
 {
   "STT": 101,
   "Name": "Quầy thuốc số 37, Công ty Cổ phần Dược phẩm Yên Bái",
   "address": "Khu II, thị trấn  Trạm Tấu, huyện Trạm Tấu, tỉnh Yên Bái",
   "Longtitude": 21.5148504,
   "Latitude": 104.4315746
 },
 {
   "STT": 102,
   "Name": "Quầy thuốc Lục Văn Dũng, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Khu 4, thị trấn Nông trường Nghĩa Lộ, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.5664941,
   "Latitude": 104.5393097
 },
 {
   "STT": 103,
   "Name": "Quầy thuốc Thành Liên, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Số  228 A, đường Tuệ Tĩnh, thị trấn Mậu A, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.879969,
   "Latitude": 104.685141
 },
 {
   "STT": 104,
   "Name": "Quầy thuốc số 1, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 45, Tổ 6A,  phường Đồng Tâm, đường Đinh Tiên Hoàng, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7242511,
   "Latitude": 104.9143849
 },
 {
   "STT": 105,
   "Name": "Quầy thuốc Phùng Thúy Hà , Công ty TNHH TMDP Thanh Phương",
   "address": "Số  732, đường Yên Ninh, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái ",
   "Longtitude": 21.7278234,
   "Latitude": 104.8990639
 },
 {
   "STT": 106,
   "Name": "Quầy thuốc Quỳnh Khoa, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 169, đường Thành Công, phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái ",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 107,
   "Name": "Quầy thuốc Vũ Thị Huệ, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 153, đường Lý Thường Kiệt, tổ 22, phường Nguyễn Thái Học, thành phố  Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.0258729,
   "Latitude": 105.8416597
 },
 {
   "STT": 108,
   "Name": "Quầy thuốc Hà Thị Kim Dung, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 1162, Tổ 2A, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 109,
   "Name": "Quầy thuốc Đỗ Kim Lĩnh, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 507 Đ. Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7049316,
   "Latitude": 104.8792865
 },
 {
   "STT": 110,
   "Name": "Quầy thuốc Lý Thị Thắm, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 118, đại lộ Nguyễn Thái Học, tổ 72, phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 111,
   "Name": "Quầy thuốc Hà Thị Thu  Hằng, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 12, tổ 43, phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 112,
   "Name": "Quầy thuốc Nguyễn Thị Lan Phương, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 5, xã Giới Phiên, thành phố Yên Bái, tỉnh Yên Bái ",
   "Longtitude": 21.6921305,
   "Latitude": 104.8892473
 },
 {
   "STT": 113,
   "Name": "Quầy thuốc Trịnh Thị Vân Anh, Công ty TNHH TMDP Thanh Phương",
   "address": "Tổ 34, phố Tuần Quán, phường Yên Ninh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7064824,
   "Latitude": 104.9024651
 },
 {
   "STT": 114,
   "Name": "Quầy thuốc Nguyễn Hải Vân, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 161B, tổ 12, phường Yên Ninh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7078096,
   "Latitude": 104.8711463
 },
 {
   "STT": 115,
   "Name": "Quầy thuốc Phạm Thúy Liễu, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 102, tổ 45, đường Thành Công, phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.7100994,
   "Latitude": 104.8792101
 },
 {
   "STT": 116,
   "Name": "Quầy thuốc Trịnh Thị Diện, Công ty TNHH TMDP Thanh Phương",
   "address": "Số  301, Tổ 14, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 117,
   "Name": "Quầy thuốc Nguyễn Thị Yến, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 175, đường Kim Đồng, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái ",
   "Longtitude": 21.7220505,
   "Latitude": 104.8937977
 },
 {
   "STT": 118,
   "Name": "Quầy thuốc Bùi Thị Diệu Mai, Công ty TNHH TMDP Thanh Phương",
   "address": "Ngõ 500, đường Hòa Bình, phường Nguyễn Phúc, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.708154,
   "Latitude": 104.868772
 },
 {
   "STT": 119,
   "Name": "Quầy thuốc Đặng Thị Hồng Nhung, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 297, đường Hòa Bình, phường Hồng Hà, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7036292,
   "Latitude": 104.8747105
 },
 {
   "STT": 120,
   "Name": "Quầy thuốc Lê Thị Tuyết Chinh, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 41Tổ 78, phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 121,
   "Name": "Quầy thuốc Nguyễn Hải  Xuân, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 652, tổ 42, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 122,
   "Name": "Quầy thuốc Đỗ Thị Ngọc Minh, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 02, tổ 01, phường Yên Ninh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7078096,
   "Latitude": 104.8711463
 },
 {
   "STT": 123,
   "Name": "Quầy thuốc Hải Huế, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 140, Đại lộ Nguyễn Thái Học, tổ 72, phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 124,
   "Name": "Quầy thuốc Đỗ Thị Thu  Hường, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 397, đường Kim Đồng, tổ 40, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 125,
   "Name": "Quầy thuốc Trần Liên Phương, Công ty TNHH TMDP Thanh Phương",
   "address": "Tổ 61, phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 126,
   "Name": "Quầy thuốc Phạm Thùy Anh, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Thanh Sơn, xã Tuy Lộc, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7193766,
   "Latitude": 104.8575198
 },
 {
   "STT": 127,
   "Name": "Quầy thuốc Nguyễn Thị  Giang, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 946, đường Điện Biên, tổ 31A, phường Đồng Tâm, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.8042309,
   "Latitude": 103.1076525
 },
 {
   "STT": 128,
   "Name": "Quầy thuốc Nhuận Hóa, Công ty TNHH TMDP Thanh Phương",
   "address": "Tổ 16, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 129,
   "Name": "Quầy thuốc Hà Phúc Khánh, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Nước Mát, xã Âu Lâu, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6879285,
   "Latitude": 104.8396248
 },
 {
   "STT": 130,
   "Name": "Quầy thuốc Trần Thị Khuyên, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 1, xã Phúc Lộc, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6720151,
   "Latitude": 104.9184443
 },
 {
   "STT": 131,
   "Name": "Quầy thuốc Lê Thị Thanh Nga, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 19, tổ 19, phường Hợp Minh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6858025,
   "Latitude": 104.8629746
 },
 {
   "STT": 132,
   "Name": "Quầy thuốc Triệu Quốc Việt, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 61, đường Nguyễn Du, phường Hồng Hà, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6976918,
   "Latitude": 104.8761104
 },
 {
   "STT": 133,
   "Name": "Quầy thuốc Hoàng Hải, Công ty TNHH TMDP Thanh Phương",
   "address": "Tổ 27, phường Đồng Tâm, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7181574,
   "Latitude": 104.9125615
 },
 {
   "STT": 134,
   "Name": "Quầy thuốc Nguyễn Thị Kim Phương, CT TNHH TM DP Thanh Phương",
   "address": "Thôn I, xã Phúc Lộc, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6720151,
   "Latitude": 104.9184443
 },
 {
   "STT": 135,
   "Name": "Quầy thuốc Hà Nhi, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 10, đường Lý thường Kiệt, phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 136,
   "Name": "Quầy thuốc Doãn Thúy Liên, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 958, đường Yên Ninh, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.7278234,
   "Latitude": 104.8990639
 },
 {
   "STT": 137,
   "Name": "Quầy thuốc Đoàn Bích Liên, Công ty TNHH TMDP Thanh Phương",
   "address": "Khu phố 2, thị trấn Cổ Phúc, huyện Trấn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.7625049,
   "Latitude": 104.8191965
 },
 {
   "STT": 138,
   "Name": "Quẩy thuốc Nguyễn Xuân Toản, CT TNHH TM DP Thanh Phương",
   "address": "Thôn 4, thị trấn Cổ Phúc, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.770117,
   "Latitude": 104.818982
 },
 {
   "STT": 139,
   "Name": "Quầy thuốc Lương Thị Huyền Hảo, CT TNHH TM DP Thanh Phương",
   "address": "Thôn 4, xã Hưng Khánh, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.5940435,
   "Latitude": 104.7639158
 },
 {
   "STT": 140,
   "Name": "Quầy thuốc Dương Thị Diệu, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn An Phú, xã Y Can, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.7498139,
   "Latitude": 104.8148194
 },
 {
   "STT": 141,
   "Name": "Quầy thuốc Nguyễn Thị Phượng, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Liên Hiệp, xã Minh Quân, huyện Trấn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.6407283,
   "Latitude": 104.9184313
 },
 {
   "STT": 142,
   "Name": "Quầy thuốc Thùy Linh, Công ty TNHH TMDP Thanh Phương",
   "address": "Khu 2, thị trấn Cổ Phúc, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.7625049,
   "Latitude": 104.8191965
 },
 {
   "STT": 143,
   "Name": "Quầy thuốc Nguyễn Thị Thúy Mạnh, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 9, xã Hưng Khánh, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.5673315,
   "Latitude": 104.7579278
 },
 {
   "STT": 144,
   "Name": "Quầy thuốc Trần Thị Thu Hương, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 5, xã Việt Thành, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.6160083,
   "Latitude": 104.798771
 },
 {
   "STT": 145,
   "Name": "Quầy thuốc Nguyễn Thị Thỏa, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 9, xã Quy Mông, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.7863145,
   "Latitude": 104.7973121
 },
 {
   "STT": 146,
   "Name": "Quầy thuốc Hoàng Thị Ngân Hàng, Công ty TNHH TMDP Thanh Phương",
   "address": "Khu chợ thôn Tân Tiến 1, xã Xuân Ái, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.828054,
   "Latitude": 104.73192
 },
 {
   "STT": 147,
   "Name": "Quầy thuốc Đào Thị Thủy, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 11, xã Lâm Giang, huyện Văn Yên",
   "Longtitude": 22.0454257,
   "Latitude": 104.5008159
 },
 {
   "STT": 148,
   "Name": "Quầy thuốc Phan Quang Việt- CT TNHH TM DP Thanh Phương",
   "address": "Thôn Trung Tâm, xã Đông Cuông, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.9487688,
   "Latitude": 104.6139172
 },
 {
   "STT": 149,
   "Name": "Quầy thuốc Đỗ Thị Hải Yến, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Cầu Khai, xã Mậu Đông, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.93525,
   "Latitude": 104.6310911
 },
 {
   "STT": 150,
   "Name": "Quầy thuốc Nguyễn Thị Mai, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Tân Tiến 1, xã Xuân Ái, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.828054,
   "Latitude": 104.73192
 },
 {
   "STT": 151,
   "Name": "Quầy thuốc Bùi Thị Thu, CT TNHH TMDP Thanh Phương",
   "address": "Thôn Trung Tâm, xã An Thịnh, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8804826,
   "Latitude": 104.6419791
 },
 {
   "STT": 152,
   "Name": "Quầy thuốc Lê Văn Bình, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Chè Vè, xã An Thịnh, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.869901,
   "Latitude": 104.6456647
 },
 {
   "STT": 153,
   "Name": "Quầy thuốc Nguyễn Thị Thùy Trinh, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 18, xã Lâm Giang, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 22.0454257,
   "Latitude": 104.5008159
 },
 {
   "STT": 154,
   "Name": "Quầy thuốc Trịnh Thị Hà, CT TNHH TM DP Thanh Phương",
   "address": "Thôn Liên Kết, xã Lang Thíp, huyện Văn Yên",
   "Longtitude": 22.1173658,
   "Latitude": 104.4169284
 },
 {
   "STT": 155,
   "Name": "Quầy thuốc Nguyễn Thùy Linh, Công ty TNHH thương mại dược phẩm Thanh Phương",
   "address": "Thôn khe Dứa, xã Viễn Sơn, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.7916014,
   "Latitude": 104.7496873
 },
 {
   "STT": 156,
   "Name": "Quầy thuốc Trịnh Văn Mạnh, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 4, Xã Quang Minh, H. Văn Yên",
   "Longtitude": 21.9926667,
   "Latitude": 104.6238048
 },
 {
   "STT": 157,
   "Name": " Quầy thuốc Phương Tén, Công ty TNHH thương mại dược phẩm Thanh Phương",
   "address": "Thôn 2, xã Đại Sơn, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.8700397,
   "Latitude": 104.6365567
 },
 {
   "STT": 158,
   "Name": "Quầy thuốc Hà Thị Ngọc, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Gốc Đa, xã Đông Cuông, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.9672373,
   "Latitude": 104.5669839
 },
 {
   "STT": 159,
   "Name": "Quầy thuốc Thanh Thúy, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 3, xã Yên Hợp, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.8789076,
   "Latitude": 104.6908517
 },
 {
   "STT": 160,
   "Name": "Quầy thuốc Nguyễn Thị Hồng, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Đồng Vật, xã An Thịnh, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8804826,
   "Latitude": 104.6412925
 },
 {
   "STT": 161,
   "Name": "Quầy thuốc Ngọc Ánh- Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 3, Xã Châu Quế Thượng, huyện Văn Yên, Tỉnh Yên Bái",
   "Longtitude": 22.0581316,
   "Latitude": 104.4141114
 },
 {
   "STT": 162,
   "Name": "Quầy thuốc Lưu Thị Hợp, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 3, Xã Yên Hợp, huyện Văn Yên, Tỉnh Yên Bái",
   "Longtitude": 21.8789076,
   "Latitude": 104.6908517
 },
 {
   "STT": 163,
   "Name": "Quầy thuốc Ngọc Trâm, CT TNHH TMDP Thanh Phương",
   "address": "Thôn Liên Kết, xã Lang Thíp, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 22.1173658,
   "Latitude": 104.4169284
 },
 {
   "STT": 164,
   "Name": "Quầy thuốc Mạnh Uyên, CT TNHH TMDP Thanh Phương",
   "address": "Thôn Tam Quan,  xã Đông An, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.9672373,
   "Latitude": 104.5669839
 },
 {
   "STT": 165,
   "Name": "Quầy thuốc Nguyễn Thị Nga- CTTNHH TMDP Thanh Phương",
   "address": "Thôn 1, xã Phong Dụ Thượng, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8103684,
   "Latitude": 104.4490398
 },
 {
   "STT": 166,
   "Name": "Quầy thuốc số 4, CT TNHH TMDP Thanh Phương",
   "address": "Tổ 2, thôn Hồng Phong, thị trấn Mậu A, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8789076,
   "Latitude": 104.6908517
 },
 {
   "STT": 167,
   "Name": "Quầy thuốc Trịnh Thị Lan Chi, CT TNHH TMDP Thanh Phương",
   "address": "Thôn 4, xã Yên Hưng, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.856683,
   "Latitude": 104.7433436
 },
 {
   "STT": 168,
   "Name": "Quầy thuốc Gia Huy, CT TNHH TM DP Thanh Phương",
   "address": "Thôn Đoàn Kết, xã Cẩm Ân, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.8603032,
   "Latitude": 104.8629746
 },
 {
   "STT": 169,
   "Name": "Quầy thuốc Lương Thủy Ánh-CT TNHH TMDP Thanh Phương",
   "address": "Tổ 15B, thị trấn Yên Bình, huyện Yên Bình, tỉnh Yên Bái ",
   "Longtitude": 21.7274186,
   "Latitude": 104.9674074
 },
 {
   "STT": 170,
   "Name": "Quầy thuốc Nguyễn Thị Thu Hà, Công ty TNHH TMDP Thanh Phương",
   "address": "Tổ 14B, thị trấnYên Bình, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.7421385,
   "Latitude": 104.9653448
 },
 {
   "STT": 171,
   "Name": "Quầy thuốc Đỗ Quỳnh Nga- CT TNHH TM DP Thanh Phương",
   "address": "Khu II, thị trấn Thác Bà, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.743216,
   "Latitude": 105.0294415
 },
 {
   "STT": 172,
   "Name": "Quầy thuốc Hoàng Hữu Tuyến- CT TNHH TM DP Thanh Phương",
   "address": "Khu I, thị trấn  Thác Bà, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.743216,
   "Latitude": 105.0294415
 },
 {
   "STT": 173,
   "Name": "Quầy thuốc Mai Thị Ngọc Ánh, Công ty TNHH thương mại và dược phẩm Thanh Phương",
   "address": "Thôn Tâm Minh, xã Mông Sơn, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.8739976,
   "Latitude": 104.9038451
 },
 {
   "STT": 174,
   "Name": "Quầy thuốc Lương Thị Hiệphường  CT TNHH TM DP Thanh Phương",
   "address": "Thôn Loan thượng, Km 15, xã Tân Hương, huyện Yên Bình, tỉnh Yên Bái. ",
   "Longtitude": 21.8296141,
   "Latitude": 104.8804893
 },
 {
   "STT": 175,
   "Name": "Quầy thuốc Cao Hải Yến, CT TNHH TM DP Thanh Phương",
   "address": "Tổ 12, thị trấn Yên Bình, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.7280564,
   "Latitude": 104.969639
 },
 {
   "STT": 176,
   "Name": "Quầy thuốc Phạm Thị Thu- CT TNHH TM DP Thanh Phương",
   "address": "Thôn Tân phong I, Xã Tân Nguyên, huyện Yên Bình, tỉnh Yên Bái ",
   "Longtitude": 21.9433754,
   "Latitude": 104.7868441
 },
 {
   "STT": 177,
   "Name": "Quầy thuốc Trần Thị Hoa, CT TNHH TMDP Thanh Phương",
   "address": "tổ 9, thị trấn Yên Bình, huyện Yên Bình, tỉnh Yên Bái(Thay đổi địa điểm kinh doanh)",
   "Longtitude": 21.7253455,
   "Latitude": 104.9710123
 },
 {
   "STT": 178,
   "Name": "Quầy thuốc Hà Thị Vững, CT TNHH TM DP Thanh Phương",
   "address": "Khu 9, thị trấn nông trường Trần Phú, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.475298,
   "Latitude": 104.7623033
 },
 {
   "STT": 179,
   "Name": "Quầy thuốc Trần Thị Anh, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Trung Tâm, xã Thượng Bằng La, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.4298431,
   "Latitude": 104.798771
 },
 {
   "STT": 180,
   "Name": "Quầy thuốc Ngô Thị Yến, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Trung Tâm, xã Bình Thuận, huyện Văn Chấn, tỉnh Yên Bái.",
   "Longtitude": 21.4341122,
   "Latitude": 104.8804893
 },
 {
   "STT": 181,
   "Name": "Quầy thuốc Vũ Thị Hồng Khánh, Công ty TNHH TMDP Thanh Phương",
   "address": "Tổ dân phố 9, thị trấn  Nông trường Trần Phú, huyện Văn Chấn, tỉnh Yên Bái ",
   "Longtitude": 21.4615524,
   "Latitude": 104.7695962
 },
 {
   "STT": 182,
   "Name": "Quầy thuốc Bùi Thanh Nga, Công ty TNHH TMDP Thanh Phương",
   "address": "Khu 1, thị trấn Nông trường Liên Sơn, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.6526665,
   "Latitude": 104.4868883
 },
 {
   "STT": 183,
   "Name": "Quầy thuốc Minh Nghĩa, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Bản Cại, xã Hạnh Sơn, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.6410554,
   "Latitude": 104.502904
 },
 {
   "STT": 184,
   "Name": "Quầy thuốc Trần Thị Hằng, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Chùa, xã Chấn Thịnh, huyện Văn Chấn, tỉnh Yên Bai",
   "Longtitude": 21.4872577,
   "Latitude": 104.8334446
 },
 {
   "STT": 185,
   "Name": "Quầy thuốc Quang Trí, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Tho, xã Nghĩa Tâm, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.4150596,
   "Latitude": 104.8337879
 },
 {
   "STT": 186,
   "Name": "Quầy thuốc Hoàng Minh Đức, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Cướm, xã Thượng Bằng La, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.4158086,
   "Latitude": 104.7768894
 },
 {
   "STT": 187,
   "Name": "Quầy thuốc An Tâm, Công ty TNHH TMDP Thanh Phương",
   "address": "Bản Phiêng 1, xã Sơn Thịnh, huyện Văn Chấn, tỉnh Yên Bái ",
   "Longtitude": 21.5703097,
   "Latitude": 104.5946633
 },
 {
   "STT": 188,
   "Name": "Quầy thuốc Nguyễn Thùy Linh, Công ty TNHH TMDP Thanh Phương",
   "address": "Số 490, tổ 4, phường Trung Tâm, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 189,
   "Name": "Quầy thuốc Bùi Thị Vân, Công ty TNHH TMDP Thanh Phương",
   "address": "Tổ 6, phường Trung Tâm, , thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 190,
   "Name": "Quầy thuốc Nguyễn Mạnh Tuấn, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Ả Thượng, xã Nghĩa Phúc, thị xã Nghĩa Lộ, tỉnh Yên Bái  ",
   "Longtitude": 21.6108145,
   "Latitude": 104.5007719
 },
 {
   "STT": 191,
   "Name": "Quầy thuốc Lù Mạnh  Toàn, Công ty TNHH TMDP Thanh Phương",
   "address": "Tổ 4, phường Cầu Thia, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.5873546,
   "Latitude": 104.5174652
 },
 {
   "STT": 192,
   "Name": "Quầy thuốc Phạm Thùy Phạm Thùy Vân, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Đêu 2, Xã Nghĩa An, Thị xã Nghĩa Lộ, Tỉnh Yên Bái",
   "Longtitude": 21.6018769,
   "Latitude": 104.5062651
 },
 {
   "STT": 193,
   "Name": "Quầy thuốc Lò Thị Kim Nguyễn, Công ty TNHH TMDP Thanh Phương",
   "address": "Tổ 12, phường Trung Tâm, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.6008989,
   "Latitude": 104.5129609
 },
 {
   "STT": 194,
   "Name": "Quầy thuốc Hà Trinh, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Ả Thượng, Xã Nghĩa Phúc, Thị xã Nghĩa Lộ, Tỉnh Yên Bái",
   "Longtitude": 21.6108145,
   "Latitude": 104.5007719
 },
 {
   "STT": 195,
   "Name": "Quầy thuốc Nông Minh Luân, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 5 Nà Quáng, xã Mường lai, huyện Lục Yên, tỉnh Yên Bái.",
   "Longtitude": 22.1596549,
   "Latitude": 104.8337879
 },
 {
   "STT": 196,
   "Name": "Quầy thuốc Hoàng Thị Xen, Công ty TNHH TMDP Thanh Phương",
   "address": "Chợ Km 38, Thôn Làng Thìu, Xã Trung tâm, huyện Lục Yên, tỉnh Yên Bái ",
   "Longtitude": 21.9799257,
   "Latitude": 104.7637619
 },
 {
   "STT": 197,
   "Name": "Quầy thuốc Nguyễn Cẩm Nhung , Công ty TNHH TMDP Thanh Phương",
   "address": "Số 291, đường Nguyễn Tất Thành, thị trấn  Yên Thế, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.1137886,
   "Latitude": 104.7730972
 },
 {
   "STT": 198,
   "Name": "Quầy thuốc Nguyễn Thị Loan, Công ty TNHH TMDP Thanh Phương",
   "address": "Tổ 1, thôn Hồng Phong, thị trấn Mậu A, huyện Văn Yên, tỉnh Yên bái",
   "Longtitude": 21.8789076,
   "Latitude": 104.6908517
 },
 {
   "STT": 199,
   "Name": "Quầy thuốc Trần Thị Hoài Thu, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 4, Xã Hưng Khánh, huyện Trấn Yên, Tỉnh Yên bái",
   "Longtitude": 21.5940435,
   "Latitude": 104.7639158
 },
 {
   "STT": 200,
   "Name": "Quầy thuốc Đỗ Thị Nhàn, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 7, xã Tân Hợp, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8751907,
   "Latitude": 104.5615895
 },
 {
   "STT": 201,
   "Name": "Quầy thuốc Vũ Văn Long, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 1, xã Phong Dụ Hạ, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.9237155,
   "Latitude": 104.4679629
 },
 {
   "STT": 202,
   "Name": "Quầy thuốc Trần Kim Lý, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 5, xã Lâm Giang, huyện Văn Yên, tỉnh Yên Bái ",
   "Longtitude": 22.0235794,
   "Latitude": 104.5232901
 },
 {
   "STT": 203,
   "Name": "Quầy thuốc Trịnh Thị Lan Chi , CTTNHH TMDP Thanh Phương",
   "address": "Đội 3, xã Đại Sơn, huyện Văn Yên",
   "Longtitude": 21.8698428,
   "Latitude": 104.5655273
 },
 {
   "STT": 204,
   "Name": "Quầy thuốc Thu Trang, Công ty TNHH TMDP Thanh Phương",
   "address": "Thông Trung Tâm, xã An Thịnh, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.8762889,
   "Latitude": 104.6918022
 },
 {
   "STT": 205,
   "Name": "Quầy thuốc Hà Thị Hồng Hiếu, Công ty TNHH TMDP Thanh Phương",
   "address": "Bản Nậm Khắt, xã Nậm Khắt, huyện Mù Cang Chải, tỉnh Yên Bái",
   "Longtitude": 21.7016339,
   "Latitude": 104.2279672
 },
 {
   "STT": 206,
   "Name": "Quầy thuốc Quang Vũ, Công ty TNHH TM DP Thanh Phương",
   "address": "Thôn Tân Tiến II, xã Xuân Ái, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8148923,
   "Latitude": 104.7229279
 },
 {
   "STT": 207,
   "Name": "Quầy thuốc Trần Văn Quang, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Khe Cỏ, xã An Thịnh, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8606091,
   "Latitude": 104.6371941
 },
 {
   "STT": 208,
   "Name": "Quầy thuốc Thu Hương, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Tĩnh Hưng, xã Hưng Khánh, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.5673315,
   "Latitude": 104.7579278
 },
 {
   "STT": 209,
   "Name": "Quầy thuốc Đinh Thị Kim Thu, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 3, xã Tân Đồng, huyện Trấn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.855155,
   "Latitude": 104.7925923
 },
 {
   "STT": 210,
   "Name": "Quầy thuốc Lê Thị Phương Thảo, CT TNHH TMDP Thanh Phương",
   "address": "Tổ 3, thị trấn Mù Cang Chải, huyện Mù Cang Chải, tỉnh Yên Bái.",
   "Longtitude": 21.8528313,
   "Latitude": 104.0836104
 },
 {
   "STT": 211,
   "Name": "Quầy thuốc Hiếu Ngoan- CT TNHH TMDP Thanh Phương",
   "address": "Thôn Cây Đa, xã An Thịnh, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.8804826,
   "Latitude": 104.6412925
 },
 {
   "STT": 212,
   "Name": "Quầy thuốc Nguyễn Thị Nhung, CT TNHH TMDP Thanh Phương",
   "address": "Thôn Khe Chung 1, xã Xuân Tầm, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.8867122,
   "Latitude": 104.5072722
 },
 {
   "STT": 213,
   "Name": "Quầy thuốc Việt Ly, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Ngọc Châu, xã Châu Quế Hạ, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.992582,
   "Latitude": 104.4956239
 },
 {
   "STT": 214,
   "Name": "Quầy thuốc Nguyễn Như Quỳnh, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn Đồng Gianh, xã Báo Đáp, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8222919,
   "Latitude": 104.7695962
 },
 {
   "STT": 215,
   "Name": "Quầy thuốc Thành Vũ, Công ty TNHH TMDP Thanh Phương",
   "address": "Thôn 7, Xã Đại Phác, huyện Văn Yên,Tỉnh Yên Bái",
   "Longtitude": 21.8455566,
   "Latitude": 104.6412925
 },
 {
   "STT": 216,
   "Name": "Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số 725 đường Yên Ninh, Phường Minh Tân, thành phố  Yên Bái, Tỉnh Yên Bái",
   "Longtitude": 21.7266671,
   "Latitude": 104.8946983
 },
 {
   "STT": 217,
   "Name": "Công ty  TNHH TMDP Cường Mùi",
   "address": "Tổ 1, thị trấn Yên Bình, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.7399062,
   "Latitude": 104.9457754
 },
 {
   "STT": 218,
   "Name": "Công ty TNHH TM DP Thanh Phương",
   "address": "Số 772B, đường Điện Biên, Tổ 45, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái ",
   "Longtitude": 21.8042309,
   "Latitude": 103.1076525
 },
 {
   "STT": 219,
   "Name": "Công ty  TNHH DP Sơn Thủy",
   "address": "509, Đ. Yên Ninh, phường Yên Ninh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7071333,
   "Latitude": 104.8720476
 },
 {
   "STT": 220,
   "Name": "Công ty CPYT AMV Hoàng Liên",
   "address": "Tổ 12A, đường Nguyễn Tất Thành, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7253742,
   "Latitude": 104.9207918
 },
 {
   "STT": 221,
   "Name": "CT  TNHH TM và DP Yên Bái",
   "address": "số 266, Đường Đinh Tiên Hoàng, Phường Yên Thịnh, thành phố Yên Bái ",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 222,
   "Name": "Công ty cổ phần Dược Lê Nguyễn",
   "address": "Số  139, đường Cao Thắng, Phường Yên Ninh,  thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.718863,
   "Latitude": 104.8868482
 },
 {
   "STT": 223,
   "Name": "Chi nhánh công ty cổ phần Traphaco tại Yên Bái",
   "address": "Số 631, đường Điện Biên, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7165819,
   "Latitude": 104.8989437
 },
 {
   "STT": 224,
   "Name": "Công ty TNHH sản xuất Y học cổ truyền và Đông Dược Thế Gia",
   "address": "Thôn Thác Hoa 3, xã Sơn Thịnh, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.5706355,
   "Latitude": 104.5942828
 },
 {
   "STT": 225,
   "Name": "Nhà thuốc Bùi Thị Lịch ",
   "address": "Số 260 đường Trần Phú, Tổ 36A, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7291001,
   "Latitude": 104.9301246
 },
 {
   "STT": 226,
   "Name": "Nhà thuốc Bình Dung",
   "address": "Số 890, đường Điện Biên, tổ 50, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.8042309,
   "Latitude": 103.1076525
 },
 {
   "STT": 227,
   "Name": "Nhà thuốc Bệnh viện 103",
   "address": "Số 494, Tổ 44,  phường Nguyễn Phúc, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.7069492,
   "Latitude": 104.8702721
 },
 {
   "STT": 228,
   "Name": "Nhà thuốc Hạnh Huế",
   "address": "số 396, Đại lộ Nguyễn Thái Học, Tổ 36 A, thành phố  Yên Bái, Tỉnh Yên Bái",
   "Longtitude": 21.7167689,
   "Latitude": 104.8985878
 },
 {
   "STT": 229,
   "Name": "Nhà Thuốc Trần Anh",
   "address": "Số 73, đường Yên Ninh, phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 230,
   "Name": "Nhà thuốc Khánh Liên",
   "address": "Số 34, đường Lê Hồng Phong, tổ 26A, phường Nguyễn Thái Học, thành phố  Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7145018,
   "Latitude": 104.8779953
 },
 {
   "STT": 231,
   "Name": "Nhà thuốc Thủy Lan",
   "address": "Số 724 đường Yên Ninh, Phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7278234,
   "Latitude": 104.8990639
 },
 {
   "STT": 232,
   "Name": "Nhà thuốc Đinh Thị Hường",
   "address": "Số 06, đường Kim Đồng, Tổ 25, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7259854,
   "Latitude": 104.8930918
 },
 {
   "STT": 233,
   "Name": "Nhà thuốc Nguyễn Thanh Liêm",
   "address": "Số 614, đường Điện Biên, tổ 41, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.8042309,
   "Latitude": 103.1076525
 },
 {
   "STT": 234,
   "Name": "Nhà thuốc Đông Hải",
   "address": "số 723, đường Yên Ninh, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7278234,
   "Latitude": 104.8990639
 },
 {
   "STT": 235,
   "Name": "Nhà thuốc Việt Tràng An",
   "address": "Số 332, đường Đinh Tiên Hoàng, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 236,
   "Name": "Nhà thuốc Bình Minh",
   "address": "Số 113, đường Hồ Xuân Hương, tổ 5, phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 237,
   "Name": "Nhà thuốc Phạm Thị  Hồng",
   "address": "Số 73 đường Yên Ninh, tổ 18A, phường Nguyễn Thái Học , thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 238,
   "Name": "Nhà thuốc Phương Thảo",
   "address": "Số  201, đường Đinh Tiên Hoàng, phường Đồng Tâm, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7267514,
   "Latitude": 104.9166724
 },
 {
   "STT": 239,
   "Name": "Nhà thuốc Sơn Quỳnh",
   "address": "Số 997, đường Điện Biên, phường Đồng Tâm, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7230251,
   "Latitude": 104.9129491
 },
 {
   "STT": 240,
   "Name": "Nhà thuốc Ngọc Trung",
   "address": "Số 40 đường Hòa Bình, Tổ 40, phường Nguyễn Thái Học, thành phố Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 241,
   "Name": "Nhà thuốc Minh Phúc",
   "address": "Số 728 đường Điện Biên, tổ 44, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.8042309,
   "Latitude": 103.1076525
 },
 {
   "STT": 242,
   "Name": "Nhà Thuốc Thanh Phương ",
   "address": "Số 772B, Đường Điện Biên, phường Đồng Tâm, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.7230251,
   "Latitude": 104.9129491
 },
 {
   "STT": 243,
   "Name": "Nhà Thuốc Long Bảo ",
   "address": "Số 784, đường Yên Ninh, tổ 16, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 244,
   "Name": "Nhà thuốc Tâm An",
   "address": "Tổ 42, đường Đinh Tiên Hoàng, phường Đồng Tâm thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7239227,
   "Latitude": 104.9141145
 },
 {
   "STT": 245,
   "Name": "Nhà thuốc Như Quỳnh",
   "address": "Số 320, tổ 11, phường Nguyễn Thái Học, thành phố  Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 246,
   "Name": "Nhà thuốc Ypharco, số 01",
   "address": "Số 725 đường Yên Ninh, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7266671,
   "Latitude": 104.8946983
 },
 {
   "STT": 247,
   "Name": "Nhà thuốc Vạn Bảo Tín 3",
   "address": "Số 02, Tổ 37, phường Hồng Hà, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6976918,
   "Latitude": 104.8761104
 },
 {
   "STT": 248,
   "Name": "Nhà thuốc Vạn Bảo Tín",
   "address": "Số 401, đường Kim Đồng, tổ 40, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 249,
   "Name": "Nhà thuốc Sơn Tuyền",
   "address": "Số  235, tổ 6A, phường Nguyễn Phúc, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.708154,
   "Latitude": 104.868772
 },
 {
   "STT": 250,
   "Name": "Nhà thuốc Nguyễn Anh Dũng",
   "address": "Số 738, đường Yên Ninh, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7278234,
   "Latitude": 104.8990639
 },
 {
   "STT": 251,
   "Name": "Nhà thuốc BVĐK tỉnh Yên Bái",
   "address": "Thôn 1, xã Phúc Lộc, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6720151,
   "Latitude": 104.9184443
 },
 {
   "STT": 252,
   "Name": "Nhà thuốc bệnh viện tâm thần",
   "address": "Tổ 24, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.7219921,
   "Latitude": 104.8979939
 },
 {
   "STT": 253,
   "Name": "Nhà thuốc Bệnh viện nội tiết, Bệnh viện nội tiết",
   "address": "Số 150, đường Yên Ninh, phường Nguyễn Thái Học, thành phố Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 254,
   "Name": "Nhà thuốc Bệnh viện Sản nhi",
   "address": "Số 721, đường Yên Ninh, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7269259,
   "Latitude": 104.9042728
 },
 {
   "STT": 255,
   "Name": "Nhà Thuốc Bình Dung ",
   "address": "Thôn Bình Lục, xã Văn Tiến, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.6789051,
   "Latitude": 104.9381112
 },
 {
   "STT": 256,
   "Name": "Nhà thuốc Việt Bảo",
   "address": "Số 233, đường Đinh Tiên Hoàng, tổ 11, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 257,
   "Name": "Nhà thuốc Hoàng Hương",
   "address": " Tổ 12 A, đường Nguyễn Tất Thành, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 258,
   "Name": "Nhà Thuốc Cúc Hiếu",
   "address": "Số  1095, đường Yên Ninh, tổ 24, phường Đồng Tâm, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7190744,
   "Latitude": 104.9126044
 },
 {
   "STT": 259,
   "Name": "Nhà Thuốc Hương Nam",
   "address": "Tổ 14A, thị trấn Yên Bình, huyện Yên Bình, tỉnh Yên Bái ",
   "Longtitude": 21.7421385,
   "Latitude": 104.9653448
 },
 {
   "STT": 260,
   "Name": "Nhà thuốc Tâm Đức",
   "address": "Tổ 13, thị trấn Yên Bình, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.725104,
   "Latitude": 104.96652
 },
 {
   "STT": 261,
   "Name": "Nhà thuốc Gia Hưng",
   "address": "Tổ 4, Thị trấn Yên Bình, huyện Yên Bình, Tỉnh Yên Bái",
   "Longtitude": 21.729677,
   "Latitude": 104.9615642
 },
 {
   "STT": 262,
   "Name": "Nhà thuốc Lê Thanh Tùng",
   "address": "Khu phố III, thị trấn Cổ Phúc, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.7556237,
   "Latitude": 104.8308695
 },
 {
   "STT": 263,
   "Name": "Nhà thuốc Nam Anh",
   "address": "Thôn Trung Nam, xã Hồng Ca, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.5905563,
   "Latitude": 104.6937674
 },
 {
   "STT": 264,
   "Name": "Nhà thuốc Thanh Thọ",
   "address": "Khu phố 5, thị trấn Cổ Phúc, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.7556237,
   "Latitude": 104.8308695
 },
 {
   "STT": 265,
   "Name": "Nhà thuốc An Bình",
   "address": "Thôn Trái Hút, xã An Bình, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.9706931,
   "Latitude": 104.5932063
 },
 {
   "STT": 266,
   "Name": "Nhà thuốc Hoàng Quốc Vinh",
   "address": "Số 144 đường Tuệ Tĩnh, thị trấn Mậu A, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8801181,
   "Latitude": 104.6859709
 },
 {
   "STT": 267,
   "Name": "Nhà thuốc Hưng Thuận",
   "address": "Tổ 14, thị trấn Yên Thế, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.1113763,
   "Latitude": 104.7696678
 },
 {
   "STT": 268,
   "Name": "Nhà thuốc Huy Hoàng",
   "address": "Tổ 13, thị trấn Yên Thế, huyện  Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.110575,
   "Latitude": 104.766679
 },
 {
   "STT": 269,
   "Name": "Nhà thuốc Bệnh viện, TTYT huyện Lục Yên",
   "address": "Tổ 13, thị trấn Yên Thế, huyện Lục Yên, tỉnh Yên Bái",
   "Longtitude": 22.110575,
   "Latitude": 104.766679
 },
 {
   "STT": 270,
   "Name": "Nhà thuốc Sơn Thủy",
   "address": "Tổ dân phố số 9, thị trấn Nông trường Trần Phú, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.4615524,
   "Latitude": 104.7695962
 },
 {
   "STT": 271,
   "Name": "Nhà thuốc Phương Nghĩa",
   "address": "Tổ dân phố số 1, thị trấn nông trường Liên Sơn, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.6526665,
   "Latitude": 104.4868883
 },
 {
   "STT": 272,
   "Name": "Nhà thuốc Bệnh viện, TTYT Văn Chấn ",
   "address": "Tổ dân phố 9, thị trấn nông trường Trần Phú, huyện Văn Chấn, tỉnh Yên Bái.",
   "Longtitude": 21.4615524,
   "Latitude": 104.7695962
 },
 {
   "STT": 273,
   "Name": "Nhà thuốc Ngọc Hằng",
   "address": "Thôn 14, xã Tân Thịnh, huyện Văn Chấn, tỉnh Yên Bái.",
   "Longtitude": 21.5080977,
   "Latitude": 104.7695962
 },
 {
   "STT": 274,
   "Name": "Nhà thuốc Hương Thắng ",
   "address": "Thôn 7, xã Tân Thịnh, huyện Văn Chấn, tỉnh Yên Bái.",
   "Longtitude": 21.4972309,
   "Latitude": 104.7660449
 },
 {
   "STT": 275,
   "Name": "Nhà thuốc Oxy xanh",
   "address": "Thôn Hồng Sơn, Xã Sơn Thịnh, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.4298431,
   "Latitude": 104.798771
 },
 {
   "STT": 276,
   "Name": "Nhà thuốc Miên Tuấn",
   "address": "Khu 2, Ngã ba Cát Thịnh, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.4508836,
   "Latitude": 104.6937674
 },
 {
   "STT": 277,
   "Name": "Nhà thuốc Hà Trinh",
   "address": "Số  311, tổ 5, phường Trung Tâm, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 278,
   "Name": "Nhà thuốc Tần Huấn",
   "address": "Số 01, tổ 19, phường Trung Tâm, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 279,
   "Name": "Nhà thuốc Nguyễn Trọng Nghĩa",
   "address": "Số 565,Tổ 01, phường Pú Trạng, Thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.599737,
   "Latitude": 104.492712
 },
 {
   "STT": 280,
   "Name": "Nhà thuốc BVĐKKVNL",
   "address": "Tổ 1, phường Pú Trạng, thị xã Nghĩa Lộ, tỉnh Yên Bái",
   "Longtitude": 21.599737,
   "Latitude": 104.492712
 },
 {
   "STT": 281,
   "Name": "Nhà Thuốc Phong Phú",
   "address": "Số  76, tổ 34, phường Yên Ninh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7078096,
   "Latitude": 104.8711463
 },
 {
   "STT": 282,
   "Name": "Nhà thuốc Bảo Trân",
   "address": "Thôn 13, xã Tân Thịnh, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.498047,
   "Latitude": 104.7681376
 },
 {
   "STT": 283,
   "Name": "Nhà Thuốc Tây Bắc 1",
   "address": "Tổ 57, phường Đồng Tâm, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7190744,
   "Latitude": 104.9126044
 },
 {
   "STT": 284,
   "Name": "Nhà Thuốc Gia Bình",
   "address": "Số  910, đường Điện Biên, tổ 31A, phường Đồng Tâm, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.8042309,
   "Latitude": 103.1076525
 },
 {
   "STT": 285,
   "Name": "Nhà thuốc Hoàng Phúc",
   "address": "Tổ 38, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 286,
   "Name": "Nhà thuốc Đăng Khoa",
   "address": "Tổ 45, phường Đồng Tâm, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7242511,
   "Latitude": 104.9143849
 },
 {
   "STT": 287,
   "Name": "Nhà thuốc Ngọc Trâm",
   "address": "Tổ dân phố 3, thị trấn Cổ phúc, huyện Trấn Yên, tỉnh Yên Bái",
   "Longtitude": 21.7625049,
   "Latitude": 104.8191965
 },
 {
   "STT": 288,
   "Name": "Nhà thuốc Tam Phúc",
   "address": "Số  397, đường Hòa Bình, phường Hồng Hà, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7036292,
   "Latitude": 104.8747105
 },
 {
   "STT": 289,
   "Name": "Nhà thuốc Thế Quân",
   "address": "Thôn phố 1, xã Đồng Khê, huyện Văn Chấn, tỉnh Yên Bái",
   "Longtitude": 21.5312275,
   "Latitude": 104.6412925
 },
 {
   "STT": 290,
   "Name": "Quầy thuốc bệnh viện,  TTYT Yên Bình",
   "address": "Thôn Thanh Bình, xã Phú Thịnh, huyện Yên Bình, tỉnh Yên Bái",
   "Longtitude": 21.7050739,
   "Latitude": 104.9710011
 },
 {
   "STT": 291,
   "Name": "Quầy thuốc BHYT, Phòng khám đa khoa Phú Thọ",
   "address": "Số 738, đường Yên Ninh, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7278234,
   "Latitude": 104.8990639
 },
 {
   "STT": 292,
   "Name": "Quầy cấp phát thuốc BHYT Việt Tràng An",
   "address": "Số 332, đường Đinh Tiên Hoàng, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 293,
   "Name": "Quầy cấp phát thuốc BHYT, Phòng khám đa khoa Hiệu Hoa YÊN BÁI",
   "address": "Số 642A, đường Điện Biên, tổ 42, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7221244,
   "Latitude": 104.9120584
 },
 {
   "STT": 294,
   "Name": "Quầy cấp phát thuốc BHYT, Việt Nga",
   "address": "Số 01 tổ 54, phường Đồng Tâm, thành phố  Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7190744,
   "Latitude": 104.9126044
 },
 {
   "STT": 295,
   "Name": "Quầy thuốc Vũ Thị  Chung, Công ty CPDP Yên Bái",
   "address": "Cổng Bệnh viện tỉnh, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7282087,
   "Latitude": 104.8924291
 },
 {
   "STT": 296,
   "Name": "Quầy thuốc Đỗ Thị  Hương, Công ty CPDP Yên Bái",
   "address": "Cổng Bệnh viện tỉnh, phường Minh Tân, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7282087,
   "Latitude": 104.8924291
 },
 {
   "STT": 297,
   "Name": "Quầy thuốc Bùi Thị Thơm, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Số 270, đường Thành Công, tổ 57, phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 298,
   "Name": "Quầy thuốc Trần Thị Hồng Nhung, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Số 322, đường Đinh Tiên Hoàng, tổ 14, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 299,
   "Name": "Quầy thuốc Trần Thị Hồng, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Số 736, Đường Yên Ninh, tổ 17, phường Minh Tân, thành phố  Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 300,
   "Name": "Quầy thuốc Nguyễn Thị Vân Hồng, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 2, phường Hợp Minh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6858025,
   "Latitude": 104.8629746
 },
 {
   "STT": 301,
   "Name": "Quầy thuốc Nguyễn Thị Thoa, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 9, phường Hợp Minh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6858025,
   "Latitude": 104.8629746
 },
 {
   "STT": 302,
   "Name": "Quầy thuốc Trần Thị Chinh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 51, Phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 303,
   "Name": "Quầy thuốc Đinh Thị Thu Hà, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Số  243, Đường Trần Hưng Đạo, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7167689,
   "Latitude": 104.8985878
 },
 {
   "STT": 304,
   "Name": "Quầy thuốc Hoàng Thị Mới, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Số 242, tổ 36A, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.7291001,
   "Latitude": 104.9301246
 },
 {
   "STT": 305,
   "Name": "Quầy thuốc Tô Kim Thanh, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số 544, tổ 55, phường Yên Ninh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7078096,
   "Latitude": 104.8711463
 },
 {
   "STT": 306,
   "Name": "Quầy thuốc Lê Thị Thanh Huệ, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số 188, đường Thành Công, tổ 50, phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 307,
   "Name": "Quầy thuốc Bùi Phương Thúy, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Thôn 1, xã Phúc Lộc, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6720151,
   "Latitude": 104.9184443
 },
 {
   "STT": 308,
   "Name": "Quầy thuốc Mai Thị Vân, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số  103, tổ 28A, phường Nguyễn Thái Học, thành phố  Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 309,
   "Name": "Quầy thuốc Đinh Thị Bút, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số 57, tổ 78, phường Nguyễn Thái Học, thành phố  Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 310,
   "Name": "Quầy thuốc Đặng Thị Bình, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Số  02, Tổ 9B phường Nguyễn Thái Học, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 311,
   "Name": "Quầy thuốc Vũ Kim Ngân, CT CPDP Yên Bái",
   "address": "Số 14, tổ 40, phường Nguyễn Phúc, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.708154,
   "Latitude": 104.868772
 },
 {
   "STT": 312,
   "Name": "Quầy thuốc Nguyễn Như Quỳnh, CT CPDP Yên Bái",
   "address": "Số 427, tổ 43,  phường Hồng Hà, thành phố Yên Bái",
   "Longtitude": 21.6976918,
   "Latitude": 104.8761104
 },
 {
   "STT": 313,
   "Name": "Quầy thuốc Nguyễn Thị Tân, Công ty cỏ phần Dược phẩm Yên Bái",
   "address": "Số701 Tổ 51Đ, đường Yên Ninh,  phường Yên Ninh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7167689,
   "Latitude": 104.8985878
 },
 {
   "STT": 314,
   "Name": "Quầy thuốc Lê Thanh Bình, Công ty cỏ phần Dược phẩm Yên Bái",
   "address": "Số  674, đường Đinh Tiên Hoàng, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 315,
   "Name": "Quầy thuốc Lê Thị Vân Anh, Công ty cỏ phần Dược phẩm Yên Bái",
   "address": "Tổ 41, phường Yên Ninh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7078096,
   "Latitude": 104.8711463
 },
 {
   "STT": 316,
   "Name": "Quầy thuốc Trần Thị Thơm, Công ty cỏ phần Dược phẩm Yên Bái",
   "address": "số 886, đường Điện Biên, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7183739,
   "Latitude": 104.911333
 },
 {
   "STT": 317,
   "Name": "Quầy thuốc Nguyễn Thị  Đức, Công ty cỏ phần Dược phẩm Yên Bái",
   "address": "Số814 Tổ 9, đường Yên Ninh,  phường Yên Ninh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7154218,
   "Latitude": 104.8894758
 },
 {
   "STT": 318,
   "Name": "Quầy thuốc Đinh Thị Vân Anh, Công ty cỏ phần Dược phẩm Yên Bái",
   "address": "Số  152, đường Trần Phú, tổ 54, phường Đồng Tâm, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7190744,
   "Latitude": 104.9126044
 },
 {
   "STT": 319,
   "Name": "Quầy thuốc Lê Thị Thu Huyền, Công ty cỏ phần Dược phẩm Yên Bái",
   "address": "Thôn Văn Quỳ, xã Văn Tiến, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.6789051,
   "Latitude": 104.9381112
 },
 {
   "STT": 320,
   "Name": "Quầy thuốc Nguyễn Thị Bích Liên, CT CPDP Yên Bái",
   "address": "Thôn II, xã Phúc Lộc, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6720151,
   "Latitude": 104.9184443
 },
 {
   "STT": 321,
   "Name": "Quầy thuốc Đoàn Thị Dương, Công ty cỏ phần Dược phẩm Yên Bái",
   "address": "Thôn Nước Mát, xã Âu Lâu, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6879285,
   "Latitude": 104.8396248
 },
 {
   "STT": 322,
   "Name": "Quầy thuốc Bùi Hùng Ngọc, Công ty cỏ phần Dược phẩm Yên Bái",
   "address": "Số 66, đ. Trần Hưng Đạo, tổ 12, phường Hồng Hà, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6999103,
   "Latitude": 104.878664
 },
 {
   "STT": 323,
   "Name": "Quầy thuốc Nguyễn Thị Tú Oanh, Công ty cỏ phần Dược phẩm Yên Bái",
   "address": "Thôn Minh Long, xã Tuy Lộc, thành phố  Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7217672,
   "Latitude": 104.8512993
 },
 {
   "STT": 324,
   "Name": "Quầy thuốc Nguyễn Thị Hương , Công ty cỏ phần Dược phẩm Yên Bái",
   "address": "Số 63, Tổ 01, Phường Yên Ninh, thành phố Yên Bái, tỉnh Yên Bái ",
   "Longtitude": 21.7154218,
   "Latitude": 104.8894758
 },
 {
   "STT": 325,
   "Name": "Quầy thuốc Nguyễn Thị Mai Hương, CT CPDP Yên Bái",
   "address": "Tổ 3A, Phường Hồng Hà, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.6965337,
   "Latitude": 104.8744938
 },
 {
   "STT": 326,
   "Name": "Quầy thuốc Ngô Thị Hoan, CT CPDP Yên Bái ",
   "address": "Tổ 9, phường Hợp Minh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6858025,
   "Latitude": 104.8629746
 },
 {
   "STT": 327,
   "Name": "Quầy thuốc Trần Thị Tuyết Mai, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Số 533, đại lộ Nguyễn Thái Học, tổ 42, phường Hồng Hà, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.6976918,
   "Latitude": 104.8761104
 },
 {
   "STT": 328,
   "Name": "Quầy thuốc Mỹ Linh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Số 27 đường Nguyễn Tất Thành, phường Yên Thịnh, thành phố Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7293824,
   "Latitude": 104.9353064
 },
 {
   "STT": 329,
   "Name": "Quầy thuốc Lê Thùy Nga, CT CPDP Yên Bái ",
   "address": "Thôn Bình Lục, xã Văn Tiến, thành phố Yên Bái, tỉnh Yên Bái.",
   "Longtitude": 21.6789051,
   "Latitude": 104.9381112
 },
 {
   "STT": 330,
   "Name": "Quầy thuốc Nguyễn Thị Phương Thúy, Công ty cổ phần dược phẩm Yên Bái",
   "address": "Tổ 1, khu phố 1, thị trấn Mậu A, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8784045,
   "Latitude": 104.6955442
 },
 {
   "STT": 331,
   "Name": "Quầy thuốc số 10, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Khu phố Trái Hút, xã An Bình, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.8698428,
   "Latitude": 104.5655273
 },
 {
   "STT": 332,
   "Name": "Quầy thuốc Phạm Thị Huệ, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Ki ốt số 6, Chợ An Thịnh, xã An Bình, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8688406,
   "Latitude": 104.6412925
 },
 {
   "STT": 333,
   "Name": "Quầy thuốc Trần Du Thủy, CT CPDP Yên Bái",
   "address": "Khu phố Trái Hút, xã An Bình, huyện  Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8698428,
   "Latitude": 104.5655273
 },
 {
   "STT": 334,
   "Name": "Quầy thuốc Lê Thị Lưu, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Tổ 3, khu phố 3, thị trấn Mậu A, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8784045,
   "Latitude": 104.6955442
 },
 {
   "STT": 335,
   "Name": "Quầy thuốc Lê Hồng Hạnh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Hồng Phong, thị trấn Mậu A, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8789076,
   "Latitude": 104.6908517
 },
 {
   "STT": 336,
   "Name": "Quầy thuốc Hoàng Thị Hiên, CT CPDP Yên Bái",
   "address": "Thôn 4, xã Yên Phú, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8450496,
   "Latitude": 104.6630311
 },
 {
   "STT": 337,
   "Name": "Quầy thuốc Nguyễn Thị Liên, CT CPDP Yên Bái",
   "address": "Số 167 đường Lý Tự Trọng, Thị trấn Mậu A, huyện Văn Yên, Tỉnh Yên Bái",
   "Longtitude": 21.880608,
   "Latitude": 104.6926185
 },
 {
   "STT": 338,
   "Name": "Quầy thuốc Văn Thị Kim Liên, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Trung Tâm, xã An Thịnh, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8804826,
   "Latitude": 104.6419791
 },
 {
   "STT": 339,
   "Name": "Quầy thuốc Hoàng Thị Thúy Hòa, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 7, xã Mậu Đông, huyện Văn Yên, tỉnh Yên Bái.",
   "Longtitude": 21.9253045,
   "Latitude": 104.6631432
 },
 {
   "STT": 340,
   "Name": "Quầy thuốc Phan Thị Minh, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Khu phố Trái Hút, xã An Bình, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8698428,
   "Latitude": 104.5655273
 },
 {
   "STT": 341,
   "Name": "Quầy thuốc Nguyễn Diệu Thu, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Số  243, tổ 3, khu 4, thị trấn Mậu A, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8783788,
   "Latitude": 104.6966832
 },
 {
   "STT": 342,
   "Name": "Quầy thuốc Trịnh Kim Nguyệt, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Ki ốt số 5, Chợ An Thịnh, xã An Thịnh, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8804826,
   "Latitude": 104.6412925
 },
 {
   "STT": 343,
   "Name": "Quầy thuốc Nguyễn Thị Khánh Ly, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Cổng Trào, xã An Thịnh, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.869901,
   "Latitude": 104.6456647
 },
 {
   "STT": 344,
   "Name": "Quầy thuốc Cầm Thị Huyền, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Cầu Khai, xã Đông Cuông, huyện Văn Yên, tỉnh Yên Bái",
   "Longtitude": 21.8698428,
   "Latitude": 104.5655273
 },
 {
   "STT": 345,
   "Name": "Quầy thuốc Tuyết Nhung, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn 7, xã Châu Quế Thượng, huyện Văn Yên, tỉnh Yên Bái ",
   "Longtitude": 22.0594044,
   "Latitude": 104.415313
 },
 {
   "STT": 346,
   "Name": "Quầy thuốc Mai Lan, Công ty cổ phần Dược phẩm Yên Bái",
   "address": "Thôn Liên Kết, xã Lang Thíp, huyện Văn Yên, tỉnh Yên Bái ",
   "Longtitude": 22.1173658,
   "Latitude": 104.4169284
 }
];