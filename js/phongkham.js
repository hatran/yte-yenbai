var dataphongkham = [
 {
   "STT": 1,
   "Name": "Phòng khám đa khoa Hữu Nghị 103 (VYên)",
   "address": "Huyện Văn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.8698428,
   "Latitude": 104.5655273
 },
 {
   "STT": 2,
   "Name": "Phòng khám đa khoa Hữu Nghị 103(LYên)",
   "address": "Huyện Lục Yên - Tỉnh Yên Bái",
   "Longtitude": 22.0900049,
   "Latitude": 104.705431
 },
 {
   "STT": 3,
   "Name": "Phòng khám đa khoa KV Ngã Ba Kim",
   "address": "Xã Púng Luông - Huyện Mù Cang Chải",
   "Longtitude": 21.7514895,
   "Latitude": 104.1742045
 },
 {
   "STT": 4,
   "Name": "Phòng khám đa khoa KV Khao Mang",
   "address": "Xã Lao Chải - Huyện Mù Cang Chải",
   "Longtitude": 21.80437,
   "Latitude": 103.98402
 },
 {
   "STT": 5,
   "Name": "Phòng khám đa khoa khu vực Sơn Thịnh",
   "address": "Xã Sơn Thịnh - Huyện Văn Chấn",
   "Longtitude": 21.5703097,
   "Latitude": 104.5946633
 },
 {
   "STT": 6,
   "Name": "Phòng khám đa khoa KV Trạm Tấu",
   "address": "Thị Trấn Trạm Têu - Huyện Trạm Têu",
   "Longtitude": 21.5148504,
   "Latitude": 104.4315746
 },
 {
   "STT": 7,
   "Name": "Công ty cổ phần phát triển Y tế Việt Tràng An (Phòng khám đa khoa Việt Tràng An)",
   "address": "Số 332, đường Đinh Tiên Hoàng, tổ 14 phường Yên Thịnh, TỈNH YÊN BÁI, tỉnh Yên Bái",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 8,
   "Name": "Công ty trách nhiệm hữu hạn một thành viên y học Minh Tâm (Phòng khám đa khoa Việt Nga)",
   "address": "Số nhà 01, đường Ngô Gia Tự, tổ 57 phường Đồng Tâm, TỈNH YÊN BÁI, tỉnh Yên Bái",
   "Longtitude": 21.7190744,
   "Latitude": 104.9126044
 },
 {
   "STT": 9,
   "Name": "Phòng khám đa khoa TN Hồng Ngọc",
   "address": "Thị Trấn Mậu A - Huyện Văn Yên",
   "Longtitude": 21.8783788,
   "Latitude": 104.6966832
 },
 {
   "STT": 10,
   "Name": "Phòng khám đa khoa tư nhân y cao Hồng Đức",
   "address": "Tổ 40 - phường Minh Tân - TP Yên Bái - tỉnh Yên Bái",
   "Longtitude": 21.721773,
   "Latitude": 104.8944023
 },
 {
   "STT": 11,
   "Name": "Phòng khám đa khoa Phú Thọ - Chi nhánh công ty cổ phần thương mại và dịch vụ Hồng Phát",
   "address": "738, 740, 742 đường Yên Ninh, phường Minh Tân, TP Yên Bái, tỉnh Yên Bái",
   "Longtitude": 21.7278234,
   "Latitude": 104.8990639
 },
 {
   "STT": 12,
   "Name": "Công ty trách nhiệm hữu hạn Medical Hiệu Hoa (Phòng khám đa khoa Hiệu Hoa - YÊN BÁI)",
   "address": "Số 642A, đường Điện Biên, tổ 42, phường Minh Tân, TỈNH YÊN BÁI, tỉnh Yên Bái",
   "Longtitude": 21.7221244,
   "Latitude": 104.9120584
 },
 {
   "STT": 13,
   "Name": "Phòng khám đa khoa KV Khánh Hòa",
   "address": "Xã Động Quan -  Lục Yên - Tỉnh Yên Bái",
   "Longtitude": 22.0815602,
   "Latitude": 104.670443
 },
 {
   "STT": 14,
   "Name": "Phòng khám đa khoa KV xã Minh Tiến",
   "address": "Xã Minh Tiến - huyện  Lục Yên - tỉnh Yên Bái",
   "Longtitude": 22.0644264,
   "Latitude": 104.8571368
 },
 {
   "STT": 15,
   "Name": "Phòng khám đa khoa KV An Bình",
   "address": "Xã An Bình - Văn Yên",
   "Longtitude": 22.0201729,
   "Latitude": 104.577181
 },
 {
   "STT": 16,
   "Name": "Phòng khám đa khoa KV Phong Dụ",
   "address": "Xã Phong Dô Thượng",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 17,
   "Name": "Phòng khám đa khoa KV Thác Bà",
   "address": "Thị Trấn Thác Bà - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.743216,
   "Latitude": 105.0294415
 },
 {
   "STT": 18,
   "Name": "Phòng khám đa khoa KV Cảm Ân",
   "address": "Xã Cảm Ân - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.8603032,
   "Latitude": 104.8629746
 },
 {
   "STT": 19,
   "Name": "Phòng khám đa khoa KV Cảm Nhân",
   "address": "Xã Cảm Nhân - huyện Yên Bình - tỉnh Yên Bái",
   "Longtitude": 21.9715666,
   "Latitude": 104.9724305
 },
 {
   "STT": 20,
   "Name": "Phòng khám đa khoa KV Tân Thịnh",
   "address": "Xã Tân Thịnh - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.5080977,
   "Latitude": 104.7695962
 },
 {
   "STT": 21,
   "Name": "Phòng khám đa khoa KV Chấn Thịnh",
   "address": "Xã Chên Thịnh - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.4848617,
   "Latitude": 104.8337879
 },
 {
   "STT": 22,
   "Name": "Phòng khám đa khoa KV Đồng Khê",
   "address": "Xã Đồng Khê - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.5312275,
   "Latitude": 104.6412925
 },
 {
   "STT": 23,
   "Name": "Phòng khám đa khoa KV Sơn Lương",
   "address": "Xã Sơn Lương - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.6844394,
   "Latitude": 104.4898001
 },
 {
   "STT": 24,
   "Name": "Phòng khám đa khoa KV Gia Hội",
   "address": "Xã Gia Hội - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.7319148,
   "Latitude": 104.4141114
 },
 {
   "STT": 25,
   "Name": "Phòng khám TTNT Chè N.Lộ",
   "address": "Thị Trấn Nt Nghĩa Lộ - huyện Văn Chấn - tỉnh Yên Bái",
   "Longtitude": 21.6018769,
   "Latitude": 104.5062651
 },
 {
   "STT": 26,
   "Name": "Phòng khám đa khoa KV Cát Thịnh",
   "address": "Xã Cát Thịnh-Huyện Văn Chấn-Tỉnh Yên Bái",
   "Longtitude": 21.4508836,
   "Latitude": 104.6937674
 },
 {
   "STT": 27,
   "Name": "Phòng khám đa khoa KV Hưng khánh",
   "address": "Xã Hưng Khánh - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.5673315,
   "Latitude": 104.7579278
 },
 {
   "STT": 28,
   "Name": "Phòng khám đa khoa KV Việt Cường",
   "address": "Xã Việt Cường - Huyện Trấn Yên - Tỉnh Yên Bái",
   "Longtitude": 21.6223504,
   "Latitude": 104.8571368
 },
 {
   "STT": 29,
   "Name": "Phòng khám Chuyên khoa Ung bướu",
   "address": "SN 742, Tổ 17, phường Minh Tân, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 30,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Ki ốt 8, Chợ Nguyễn Thái Học, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7167689,
   "Latitude": 104.8985878
 },
 {
   "STT": 31,
   "Name": "Phòng khám Nội tổng hợp Yên Hà Family",
   "address": "SN 776, đường Yên Ninh, tổ 16, phường Minh Tân, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 32,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "SN 271, Đường Kim Đồng, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.720019,
   "Latitude": 104.8965885
 },
 {
   "STT": 33,
   "Name": "Dịch vụ làm Răng giả",
   "address": "Tổ 14, phường Yên Thịnh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 34,
   "Name": "Phòng khám Chuyên khoa Tai - Mũi - Họng",
   "address": "Số nhà 566, Đường Điện Biên, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7163188,
   "Latitude": 104.9104059
 },
 {
   "STT": 35,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "SN 703, Đường Điện Biên, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7163188,
   "Latitude": 104.9104059
 },
 {
   "STT": 36,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Tổ 31, phường Yên Thịnh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 37,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "SN 27, Đường Thành Công, \nphường Nguyễn Thái Học, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 38,
   "Name": "Phòng khám Chuyên khoa Da liễu",
   "address": "SN 836, Đường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.717994,
   "Latitude": 104.8806898
 },
 {
   "STT": 39,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "SN 80, phường Đồng Tâm, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7230251,
   "Latitude": 104.9129491
 },
 {
   "STT": 40,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "SN 166, Đường Kim Đồng, \nphường Minh Tân, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7226181,
   "Latitude": 104.8927034
 },
 {
   "STT": 41,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "SN 186, đường Đinh Tiên Hoàng, thành phố, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7221244,
   "Latitude": 104.9120584
 },
 {
   "STT": 42,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Thôn 1, Hợp Minh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.6858025,
   "Latitude": 104.8629746
 },
 {
   "STT": 43,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "SN 53, Đường Quang Trung, \nphường Minh Tân, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7215964,
   "Latitude": 104.9070656
 },
 {
   "STT": 44,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "SN 54, Đường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7154218,
   "Latitude": 104.8894758
 },
 {
   "STT": 45,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "Tổ 22, phường Hồng Hà, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.6976918,
   "Latitude": 104.8761104
 },
 {
   "STT": 46,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "SN 805, Đường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.717994,
   "Latitude": 104.8806898
 },
 {
   "STT": 47,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "SN 541, Đường Điện Biên, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7168259,
   "Latitude": 104.8987537
 },
 {
   "STT": 48,
   "Name": "Phòng khám Chuyên khoa Tai - Mũi - Họng",
   "address": "SN 96, Đường Kim Đồng, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7241938,
   "Latitude": 104.8931265
 },
 {
   "STT": 49,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp",
   "address": "SN 382, Đường Hòa Bình, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7167689,
   "Latitude": 104.8985878
 },
 {
   "STT": 50,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Tổ 45, phườngNguyễn Thái Học, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 51,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "SN 257, Đường Quang Trung, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7225826,
   "Latitude": 104.9070386
 },
 {
   "STT": 52,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp",
   "address": "Thôn 9, Hợp Minh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.6858025,
   "Latitude": 104.8629746
 },
 {
   "STT": 53,
   "Name": "Phòng khám Chuyên khoa Ngoại",
   "address": "SN 555, Đường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.717994,
   "Latitude": 104.8806898
 },
 {
   "STT": 54,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "SN 864, Đường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.717994,
   "Latitude": 104.8806898
 },
 {
   "STT": 55,
   "Name": "Dịch vụ Cấp cứu, hỗ trợ vận chuyển người bệnh",
   "address": "Tổ 53, phường Minh Tân, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.723886,
   "Latitude": 104.9049594
 },
 {
   "STT": 56,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Tổ 6, phường Nguyễn Thái Học, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 57,
   "Name": "Dịch vụ Kính thuốc",
   "address": "Ki ốt 16, 18, Tầng 1, chợ ga Yên Bái, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7033482,
   "Latitude": 104.8818957
 },
 {
   "STT": 58,
   "Name": "Phòng khám Chuyên khoa Tai - Mũi - Họng",
   "address": "Tổ 40, phường Minh Tân, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7156731,
   "Latitude": 104.9047449
 },
 {
   "STT": 59,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "Tổ 3, phường Đồng Tâm, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7230251,
   "Latitude": 104.9129491
 },
 {
   "STT": 60,
   "Name": "Phòng khám Chuyên khoa Da liễu",
   "address": "Tổ 62, phường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7078096,
   "Latitude": 104.8711463
 },
 {
   "STT": 61,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Tổ 15, phường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7078096,
   "Latitude": 104.8711463
 },
 {
   "STT": 62,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Tổ 32C, phường Đồng Tâm, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7212273,
   "Latitude": 104.9143639
 },
 {
   "STT": 63,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp",
   "address": "Tổ 16, phường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7078096,
   "Latitude": 104.8711463
 },
 {
   "STT": 64,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Tổ 76, phườngNguyễn Thái Học, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 65,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Tổ 17, phường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7078096,
   "Latitude": 104.8711463
 },
 {
   "STT": 66,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "SN 10, Đường Tô Hiệu, \nphường Đồng Tâm, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7230251,
   "Latitude": 104.9129491
 },
 {
   "STT": 67,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "SN122, Đường Thành Công, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7143577,
   "Latitude": 104.8794629
 },
 {
   "STT": 68,
   "Name": "Dịch vụ Xoa bóp",
   "address": "Thôn 9, Xã Hợp Minh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.693344,
   "Latitude": 104.867577
 },
 {
   "STT": 69,
   "Name": "Phòng khám Chuyên khoa Ngoại",
   "address": "Tổ 27B, phường Nguyễn Phúc, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.708154,
   "Latitude": 104.868772
 },
 {
   "STT": 70,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Tổ 61, phường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7078096,
   "Latitude": 104.8711463
 },
 {
   "STT": 71,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Ki ốt, Chợ Nguyễn Thái Học, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7119663,
   "Latitude": 104.8790296
 },
 {
   "STT": 72,
   "Name": "Dịch vụ Kính thuốc",
   "address": "SN 297, Đường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7154218,
   "Latitude": 104.8894758
 },
 {
   "STT": 73,
   "Name": "Phòng khám Đa khoa Việt Tràng An",
   "address": "SN 332, đường Đinh Tiên Hoàng, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7267514,
   "Latitude": 104.9166724
 },
 {
   "STT": 74,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "SN 61, Đường Nguyễn Du, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7167689,
   "Latitude": 104.8985878
 },
 {
   "STT": 75,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Số 2, Đường Lê Văn Tám, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7253378,
   "Latitude": 104.9141574
 },
 {
   "STT": 76,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "SN 227, Đường Thanh Niên, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.6967104,
   "Latitude": 104.8773712
 },
 {
   "STT": 77,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Tổ 5, phường Yên Thịnh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7318483,
   "Latitude": 104.9292424
 },
 {
   "STT": 78,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Số 56, Tổ 28, phường Minh Tân, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 79,
   "Name": "Phòng khám Sản phụ khoa Eva",
   "address": "Số 31, Tổ 11, phường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7078096,
   "Latitude": 104.8711463
 },
 {
   "STT": 80,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "SN 01, Đường Trần Quốc Toản, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.718801,
   "Latitude": 104.9102809
 },
 {
   "STT": 81,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp",
   "address": "SN 230, Đường Điện Biên, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7230251,
   "Latitude": 104.9129491
 },
 {
   "STT": 82,
   "Name": "Phòng khám Đa khoa Y cao Hồng Đức",
   "address": "SN 533, Đường Điện Biên, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7163188,
   "Latitude": 104.9104059
 },
 {
   "STT": 83,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "Tổ 1A, phường Đồng Tâm, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7230251,
   "Latitude": 104.9129491
 },
 {
   "STT": 84,
   "Name": "Phòng xét nghiệm Dung Bình",
   "address": "SN 641, Đường Yên Ninh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7154218,
   "Latitude": 104.8894758
 },
 {
   "STT": 85,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "SN06, đường Thành Công, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7143577,
   "Latitude": 104.8794629
 },
 {
   "STT": 86,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "SN215, đường Điện Biên, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7163188,
   "Latitude": 104.9104059
 },
 {
   "STT": 87,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "SN 152, đường Đinh Tiên Hoàng, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7221244,
   "Latitude": 104.9120584
 },
 {
   "STT": 88,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "SN 01, đường Trần Quốc Toản, tổ 32B, phường Đồng Tâm, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.719797,
   "Latitude": 104.9094179
 },
 {
   "STT": 89,
   "Name": "Phòng khám Chuyên khoa Da liễu",
   "address": "SN 66, đường Thanh Liêm, tổ 1,\nphường Yên Thịnh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7285957,
   "Latitude": 104.9162305
 },
 {
   "STT": 90,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "SN14, đườngPhan Đăng Lưu,\nphường Yên Thịnh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7245988,
   "Latitude": 104.8913277
 },
 {
   "STT": 91,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Tổ 25, phường Hồng Hà, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.696346,
   "Latitude": 104.8731922
 },
 {
   "STT": 92,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "SN 670, đường Điện Biên, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7162677,
   "Latitude": 104.8967099
 },
 {
   "STT": 93,
   "Name": "Phòng khám Chuyên khoa Phụ sản",
   "address": "SN 58 , Tổ 26, Phường Đồng\nTâm, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7230251,
   "Latitude": 104.9129491
 },
 {
   "STT": 94,
   "Name": "Phòng khám Chuyên khoa Tâm thần",
   "address": "SN 294, đường Điện Biên, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7163188,
   "Latitude": 104.9104059
 },
 {
   "STT": 95,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Tổ 58, phường Nguyễn Thái học\nYên Bái, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 96,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Tổ 17, phường Minh Tân Yên Bái, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 97,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Tổ 36b, phường Nguyễn Thái học\nYên Bái, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 98,
   "Name": "Phòng khám Nha khoa Hiệu Hoa",
   "address": "SN 533, đường Điện Biên, tổ 39, phường\nMinh Tân, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7221244,
   "Latitude": 104.9120584
 },
 {
   "STT": 99,
   "Name": "Phòng khám Đa khoa Việt Nga",
   "address": "SN 01, đường Ngô Gia Tự, tổ 57,\nphường Đồng Tâm,, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.724908,
   "Latitude": 104.9113378
 },
 {
   "STT": 100,
   "Name": "Phòng khám chuyên khoa Tai - Mũi - Họng",
   "address": "SN 670, đường Điện Biên, tổ 38, phường Hồng Hà, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.8042309,
   "Latitude": 103.1076525
 },
 {
   "STT": 101,
   "Name": "Mắt Đức Dũng",
   "address": "Tổ 50, Phường Đồng Tâm, \nTỈNH YÊN BÁI, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7230251,
   "Latitude": 104.9129491
 },
 {
   "STT": 102,
   "Name": "Phòng khám nha khoa YÊN BÁI",
   "address": "674, đường Điện Biên, tổ 43, phường Minh Tân, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7221244,
   "Latitude": 104.9120584
 },
 {
   "STT": 103,
   "Name": "Phòng khám Đa khoa Phú Thọ",
   "address": "SN 738, 740, 742, Đường Yên\nNinh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7154218,
   "Latitude": 104.8894758
 },
 {
   "STT": 104,
   "Name": "Phòng khám Nha khoa Việt Mỹ",
   "address": "Số nhà 193, Tổ 77, phường Nguyễn Thái Học, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7108553,
   "Latitude": 104.8772478
 },
 {
   "STT": 105,
   "Name": "Phòng khám Răng Bẩy Yên",
   "address": "Phường Minh Tân, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 106,
   "Name": "Phòng khám Chuyên khoa Ung bướu",
   "address": "218, Đ Thành Công, P Nguyễn Thái Học, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7116677,
   "Latitude": 104.8782885
 },
 {
   "STT": 107,
   "Name": "Phòng khám Chuyên khoa Da liễu",
   "address": "230, Đ Trần Bình Trọng, P Nam Cường, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7272569,
   "Latitude": 104.8705809
 },
 {
   "STT": 108,
   "Name": "Phòng khám Chuyên khoa Sản phụ",
   "address": "SN 374, đg Hòa Bình, P Nguyễn\nPhúc, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7044708,
   "Latitude": 104.8742706
 },
 {
   "STT": 109,
   "Name": "Dịch vụ Y tế tư nhân Hoàn",
   "address": " Long Thành, Tuy Lộc, thành phố Yên\nBái, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7217672,
   "Latitude": 104.8512993
 },
 {
   "STT": 110,
   "Name": "Phòng khám Đa khoa Hiệu Hoa",
   "address": "SN 642A, Đường Điện Biên, phường Minh Tân, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7165986,
   "Latitude": 104.8989212
 },
 {
   "STT": 111,
   "Name": "Phòng khám Nha khoa Thanh Hà",
   "address": "Tổ 09, phường Hợp Minh, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.6976749,
   "Latitude": 104.858991
 },
 {
   "STT": 112,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Tổ 8, phường Minh Tân, TỈNH YÊN BÁI, Yên Bái",
   "Longtitude": 21.7259591,
   "Latitude": 104.9009254
 },
 {
   "STT": 113,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Thôn Đoàn Kết, Xã Cảm Ân, Yên Bình, Yên Bái",
   "Longtitude": 21.8603032,
   "Latitude": 104.8629746
 },
 {
   "STT": 114,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Tổ 3, thị trấn Yên Bình, Yên Bình, Yên Bái",
   "Longtitude": 21.740583,
   "Latitude": 104.9428094
 },
 {
   "STT": 115,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Tổ 18, thị trấn Yên Bình, Yên Bình, Yên Bái",
   "Longtitude": 21.7383426,
   "Latitude": 104.9778458
 },
 {
   "STT": 116,
   "Name": "Dịch vụ Y tế",
   "address": "Tổ 9, thị trấn Yên Bình, Yên Bình, Yên Bái",
   "Longtitude": 21.740583,
   "Latitude": 104.9428094
 },
 {
   "STT": 117,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Tổ 3, thị trấn Yên Bình, Yên Bình, Yên Bái",
   "Longtitude": 21.740583,
   "Latitude": 104.9428094
 },
 {
   "STT": 118,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Tổ 13, thị trấn Yên Bình, Yên Bình, Yên Bái",
   "Longtitude": 21.725104,
   "Latitude": 104.96652
 },
 {
   "STT": 119,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Tổ 13, thị trấn Yên Bình, Yên Bình, Yên Bái",
   "Longtitude": 21.725104,
   "Latitude": 104.96652
 },
 {
   "STT": 120,
   "Name": "Phòng khám Chuyên khoa Phụ sản",
   "address": "Tổ 10, thị trấn Yên Bình, Yên Bình, Yên Bái",
   "Longtitude": 21.728457,
   "Latitude": 104.9580118
 },
 {
   "STT": 121,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "Tổ 11, thị trấn Yên Bình, Yên Bình, Yên Bái",
   "Longtitude": 21.7263344,
   "Latitude": 104.9657822
 },
 {
   "STT": 122,
   "Name": "Dịch vụ trồng Răng giả",
   "address": "Tổ 9, thị trấn Yên Bình, Yên Bình, Yên Bái",
   "Longtitude": 21.740583,
   "Latitude": 104.9428094
 },
 {
   "STT": 123,
   "Name": "Phòng khám Nội tổng hợp",
   "address": "Tổ 10, thị trấn Yên Bình, Yên Bình, Yên Bái",
   "Longtitude": 21.728457,
   "Latitude": 104.9580118
 },
 {
   "STT": 124,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Tổ 1, thị trấn Yên Bình, Yên Bình, Yên Bái",
   "Longtitude": 21.7403668,
   "Latitude": 104.9438095
 },
 {
   "STT": 125,
   "Name": "Phòng khám Chuyên khoa RHM",
   "address": "Khu 1, thị trấn Thác Bà, Yên Bình, Yên Bái",
   "Longtitude": 21.7490294,
   "Latitude": 105.0294415
 },
 {
   "STT": 126,
   "Name": "Phòng khám Nội tổng hợp Đức Cường",
   "address": "Khu 4, thị trấn Thác Bà, Yên Bình, Yên Bái",
   "Longtitude": 21.7421312,
   "Latitude": 105.0519726
 },
 {
   "STT": 127,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Thôn 4, thị trấn Cổ Phúc, Trấn Yên, Yên Bái",
   "Longtitude": 21.7561541,
   "Latitude": 104.8235389
 },
 {
   "STT": 128,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Thôn 1, Xã Đào Thịnh, Trấn Yên, Yên Bái",
   "Longtitude": 21.589367,
   "Latitude": 104.7830486
 },
 {
   "STT": 129,
   "Name": "Dịch vụ Y tế",
   "address": "Thôn Ninh Phúc, Xã Nga Quán, Trấn Yên, Yên Bái",
   "Longtitude": 21.7487421,
   "Latitude": 104.8425433
 },
 {
   "STT": 130,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Chợ Vân Hội, Xã Vân Hội, Trấn Yên, Yên Bái",
   "Longtitude": 21.594335,
   "Latitude": 104.8672485
 },
 {
   "STT": 131,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Thôn Đình Xây, Xã Báo Đáp, Trấn Yên, Yên Bái",
   "Longtitude": 21.8222919,
   "Latitude": 104.7695962
 },
 {
   "STT": 132,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "Khu phố 3, thị trấn Cổ Phúc, Trấn Yên, Yên Bái",
   "Longtitude": 21.7556237,
   "Latitude": 104.8308695
 },
 {
   "STT": 133,
   "Name": "Phòng khám Nội tổng hợp",
   "address": "Tổ 4, khu phố 8, thị trấn Cổ Phúc, Trấn Yên, Yên Bái",
   "Longtitude": 21.7625049,
   "Latitude": 104.8191965
 },
 {
   "STT": 134,
   "Name": "Dịch vụ Y tế",
   "address": "Thôn 7, Vân Hội , Trấn Yên, Trấn Yên, Yên Bái",
   "Longtitude": 21.5934271,
   "Latitude": 104.8658324
 },
 {
   "STT": 135,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Tổ 6, khu 3, thị trấn Mậu A, Văn Yên, Yên Bái",
   "Longtitude": 21.8783788,
   "Latitude": 104.6966832
 },
 {
   "STT": 136,
   "Name": "Dịch vụ Y tế",
   "address": "Tổ 1, Khu 1, thị trấn Mậu A, Văn Yên, Yên Bái",
   "Longtitude": 21.8785978,
   "Latitude": 104.6972196
 },
 {
   "STT": 137,
   "Name": "Phòng khám Chuyên khoa Da liễu",
   "address": "SN 12, tổ 2, khu phố3, thị trấn Mậu A, Văn Yên, Yên Bái",
   "Longtitude": 21.8783788,
   "Latitude": 104.6966832
 },
 {
   "STT": 138,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "SN 228, đường Tuệ tĩnh, thị trấn Mậu A, Văn Yên, Yên Bái",
   "Longtitude": 21.8801173,
   "Latitude": 104.6858541
 },
 {
   "STT": 139,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Thôn Khe cỏ, xã An Thịnh,, Văn Yên, Yên Bái",
   "Longtitude": 21.8606091,
   "Latitude": 104.6371941
 },
 {
   "STT": 140,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "SN131,thôn Hồng Phong, thị trấn Mậu, Văn Yên, Yên Bái",
   "Longtitude": 21.8783486,
   "Latitude": 104.6858766
 },
 {
   "STT": 141,
   "Name": "Phòng khám Đa khoa Hồng Hà",
   "address": "Tổ 4, Khu 3, thị trấn Mậu A, Văn Yên, Yên Bái",
   "Longtitude": 21.875086,
   "Latitude": 104.692778
 },
 {
   "STT": 142,
   "Name": "Dịch vụ Y tế",
   "address": "Thôn Gốc Quân, Đông Cuông, Văn Yên, Yên Bái",
   "Longtitude": 21.9330737,
   "Latitude": 104.6068168
 },
 {
   "STT": 143,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Trái Hút, An Bình, Văn Yên, Yên Bái",
   "Longtitude": 21.9706931,
   "Latitude": 104.5932063
 },
 {
   "STT": 144,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "SN 179, Tổ 1, Thôn Hồng Phong\n, Mậu A, Văn Yên, Yên Bái",
   "Longtitude": 21.8784783,
   "Latitude": 104.6839598
 },
 {
   "STT": 145,
   "Name": "Phòng khám Đa khoa 103",
   "address": "Khu phố 2, thị trấn Mậu A, Văn Yên, Yên Bái",
   "Longtitude": 21.8784045,
   "Latitude": 104.6955442
 },
 {
   "STT": 146,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "Tổ1, Thôn Hồng Phong, Mậu A, Văn Yên, Yên Bái",
   "Longtitude": 21.8784783,
   "Latitude": 104.6839598
 },
 {
   "STT": 147,
   "Name": "Dịch vụ làm răng giả",
   "address": "Số 167, đường Tuệ tĩnh, Mậu A, Văn Yên, Yên Bái",
   "Longtitude": 21.8801173,
   "Latitude": 104.6858541
 },
 {
   "STT": 148,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "SN 61, thôn Cầu A, thị trấn Mậu A, Văn Yên, Yên Bái",
   "Longtitude": 21.8783788,
   "Latitude": 104.6966832
 },
 {
   "STT": 149,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "Thôn Trái hút, xã An Bình, Văn Yên, Yên Bái",
   "Longtitude": 21.9699924,
   "Latitude": 104.5925545
 },
 {
   "STT": 150,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình Ngọc Minh",
   "address": "Tổ 1, thôn Hồng Phong, thị trấn Mậu A, Văn Yên, Yên Bái",
   "Longtitude": 21.8789076,
   "Latitude": 104.6908517
 },
 {
   "STT": 151,
   "Name": "Dịch vụ y tế tư nhân",
   "address": "Thôn Trung tâm, xã An Thịnh, Văn Yên, Yên Bái",
   "Longtitude": 21.8804826,
   "Latitude": 104.6419791
 },
 {
   "STT": 152,
   "Name": "Dịch vụ y tế tư nhân Ngọc \nBích",
   "address": "Thôn Gốc Đa, xã Đông Cuông, Văn Yên, Yên Bái",
   "Longtitude": 21.9672373,
   "Latitude": 104.5669839
 },
 {
   "STT": 153,
   "Name": "Phòng khám chuyên khoa Nội - Nhi",
   "address": "SN 37, khu Cầu A, thị trấn Mậu A, Văn Yên, Yên Bái",
   "Longtitude": 21.8783788,
   "Latitude": 104.6966832
 },
 {
   "STT": 154,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Thôn Cầu Thia, Phù Nham, Văn Chấn, Yên Bái",
   "Longtitude": 21.6231036,
   "Latitude": 104.5247463
 },
 {
   "STT": 155,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Thôn Bản Tho, Nghĩa Tâm, Văn Chấn, Yên Bái",
   "Longtitude": 21.4150596,
   "Latitude": 104.8337879
 },
 {
   "STT": 156,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Khu 4, TTNT Nghĩa Lộ, Văn Chấn, Yên Bái",
   "Longtitude": 21.4298431,
   "Latitude": 104.798771
 },
 {
   "STT": 157,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Khu Nhà máy, TTNT. Trần Phú, Văn Chấn, Yên Bái",
   "Longtitude": 21.4820711,
   "Latitude": 104.7265077
 },
 {
   "STT": 158,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Thôn 13, Tân Thịnh, Văn Chấn, Yên Bái",
   "Longtitude": 21.508551,
   "Latitude": 104.760729
 },
 {
   "STT": 159,
   "Name": "Phòng khám Chuyên khoa Phụ sản",
   "address": "TTNT Trần Phú, Văn Chấn, Yên Bái",
   "Longtitude": 21.4615524,
   "Latitude": 104.7695962
 },
 {
   "STT": 160,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp",
   "address": "Tổ 9, NT Trần Phú, Văn Chấn, Yên Bái",
   "Longtitude": 21.4769171,
   "Latitude": 104.7359075
 },
 {
   "STT": 161,
   "Name": "Dịch vụ Y tế tư nhân",
   "address": "Xã Bình Thuận, Văn Chấn, Yên Bái",
   "Longtitude": 21.4341122,
   "Latitude": 104.8804893
 },
 {
   "STT": 162,
   "Name": "Dịch vụ Y tế tư nhân",
   "address": "Thôn 13, Xã Tân Thịnh, Văn Chấn, Yên Bái",
   "Longtitude": 21.508551,
   "Latitude": 104.760729
 },
 {
   "STT": 163,
   "Name": "Phòng khám Đa khoa y cao YÊN BÁI",
   "address": "Phố Cửa Nhì, xã Sơn Thịnh, Văn Chấn, Yên Bái",
   "Longtitude": 21.4298431,
   "Latitude": 104.798771
 },
 {
   "STT": 164,
   "Name": "Phòng khám chuyên khoa Tai - Mũi - Họng",
   "address": "Tổ dân phố nhà máy, Thị trấn Nông trường Trần Phú, Văn Chấn, Yên Bái",
   "Longtitude": 21.4808607,
   "Latitude": 104.7281123
 },
 {
   "STT": 165,
   "Name": "Dịch vụ y tế tư nhân",
   "address": "Chùa I, xã Chấn Thịnh, Văn Chấn, Yên Bái",
   "Longtitude": 21.4859971,
   "Latitude": 104.8335922
 },
 {
   "STT": 166,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Tổ 6, thị trấn Yên Thế, Lục Yên, Yên Bái",
   "Longtitude": 22.1121218,
   "Latitude": 104.7677171
 },
 {
   "STT": 167,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Tổ 4, thị trấn Yên Thế, Lục Yên, Yên Bái",
   "Longtitude": 22.1113372,
   "Latitude": 104.765444
 },
 {
   "STT": 168,
   "Name": "Phòng khám Chuyên khoa Tai - Mũi - Họng",
   "address": "Tổ 5, thị trấn Yên Thế, Lục Yên, Yên Bái",
   "Longtitude": 22.110575,
   "Latitude": 104.766679
 },
 {
   "STT": 169,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Tổ 2, thị trấn Yên Thế, Lục Yên, Yên Bái",
   "Longtitude": 22.110575,
   "Latitude": 104.766679
 },
 {
   "STT": 170,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "Tổ 4, thị trấn Yên Thế, Lục Yên, Yên Bái",
   "Longtitude": 22.1113372,
   "Latitude": 104.765444
 },
 {
   "STT": 171,
   "Name": "Dịch vụ Y tế",
   "address": "Tổ 8, thị trấn Yên Thế, Lục Yên, Yên Bái",
   "Longtitude": 22.1113372,
   "Latitude": 104.765444
 },
 {
   "STT": 172,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Khu 5, thị trấn Yên Thế, Lục Yên, Yên Bái",
   "Longtitude": 22.110575,
   "Latitude": 104.766679
 },
 {
   "STT": 173,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Tổ 5, thị trấn Yên Thế, Lục Yên, Yên Bái",
   "Longtitude": 22.110575,
   "Latitude": 104.766679
 },
 {
   "STT": 174,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "Tổ 1, Thị trấn Yên Thế, Lục Yên, Yên Bái",
   "Longtitude": 22.110575,
   "Latitude": 104.766679
 },
 {
   "STT": 175,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp",
   "address": "Tổ 13, thị trấn Yên Thế, Lục Yên, Yên Bái",
   "Longtitude": 22.110575,
   "Latitude": 104.766679
 },
 {
   "STT": 176,
   "Name": "Dịch vụ Y tế",
   "address": "Xã Mường Lai, Lục Yên, Yên Bái",
   "Longtitude": 22.1596549,
   "Latitude": 104.8337879
 },
 {
   "STT": 177,
   "Name": "Phòng khám Chuyên khoa Tai - Mũi - Họng",
   "address": "Tổ 13, thị trấn Yên Thế, Lục Yên, Yên Bái",
   "Longtitude": 22.110575,
   "Latitude": 104.766679
 },
 {
   "STT": 178,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Thôn Trung Sơn, xã Mai Sơn, Lục Yên, Yên Bái",
   "Longtitude": 22.1873306,
   "Latitude": 104.7229279
 },
 {
   "STT": 179,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "Tổ 14, thị trấn Yên Thế, Lục Yên, Yên Bái",
   "Longtitude": 22.1113763,
   "Latitude": 104.7696678
 },
 {
   "STT": 180,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Tổ 18, phường Trung Tâm, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 181,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp",
   "address": "Tổ 01, phường Pú Trạng, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.599737,
   "Latitude": 104.492712
 },
 {
   "STT": 182,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "SN 40, tổ 6, phường Tân An, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.5928826,
   "Latitude": 104.50436
 },
 {
   "STT": 183,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "SN 385, Đường Hoàng Liên Sơn, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.6004413,
   "Latitude": 104.5133937
 },
 {
   "STT": 184,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Tổ 17, phường Trung Tâm, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.6082609,
   "Latitude": 104.498712
 },
 {
   "STT": 185,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Tổ 11, phường Tân An, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.6025153,
   "Latitude": 104.5028319
 },
 {
   "STT": 186,
   "Name": "Phòng khám Chuyên khoa Tai - Mũi - Họng",
   "address": "Tổ 10, phường Trung Tâm, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.6082609,
   "Latitude": 104.5014586
 },
 {
   "STT": 187,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "Tổ 8, phường Trung Tâm, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 188,
   "Name": "Phòng khám Chuyên khoa Chẩn đoán hình ảnh",
   "address": "Tổ 4, phường Trung Tâm, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.6038313,
   "Latitude": 104.5106221
 },
 {
   "STT": 189,
   "Name": "Phòng khám Chuyên khoa Răng - Hàm - Mặt",
   "address": "Tổ 2, phường Pú Trạng, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.599737,
   "Latitude": 104.492712
 },
 {
   "STT": 190,
   "Name": "Phòng khám Chuyên khoa Da liễu",
   "address": "Tổ 16, phường Trung Tâm, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.6018769,
   "Latitude": 104.5062651
 },
 {
   "STT": 191,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Tổ 3, phường Trung Tâm, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 192,
   "Name": "Phòng khám Chuyên khoa Phụ sản - Kế hoạch hoá gia đình",
   "address": "Tổ 1, phường Tân An, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.5928826,
   "Latitude": 104.50436
 },
 {
   "STT": 193,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Ki ốt 86, Chợ Mường Lò\nphường Trung Tâm, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.6055985,
   "Latitude": 104.5087283
 },
 {
   "STT": 194,
   "Name": "Dịch vụ trồng răng giả",
   "address": "Tổ 15, phường Trung Tâm, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.6018769,
   "Latitude": 104.5062651
 },
 {
   "STT": 195,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Thôn Ả Thượng, xã Nghĩa Phúc, thị xã Nghĩa Lộ, Yên Bái",
   "Longtitude": 21.6108145,
   "Latitude": 104.5007719
 },
 {
   "STT": 196,
   "Name": "Dịch vụ Y tế",
   "address": "SN 106, Tổ 3, thị trấn Mù Căng\nChải, Mù Cang Chải, Yên Bái",
   "Longtitude": 21.8528313,
   "Latitude": 104.0836104
 },
 {
   "STT": 197,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Tổ 9, Thị trấn Mù Cang Chải, Mù Cang Chải, Yên Bái",
   "Longtitude": 21.8439811,
   "Latitude": 104.0959231
 },
 {
   "STT": 198,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp",
   "address": "Tổ 1, thị trấn Mù Cang Chải,\nhuyện Mù Cang Chải, Mù Cang Chải, Yên Bái",
   "Longtitude": 21.8519401,
   "Latitude": 104.089471
 },
 {
   "STT": 199,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Tổ 4, thị trấn Mù Cang Chải, Mù Cang Chải, Yên Bái",
   "Longtitude": 21.8519401,
   "Latitude": 104.089471
 },
 {
   "STT": 200,
   "Name": "Phòng khám Nội tổng hợp",
   "address": "Khu 1, thị trấn Trạm Tấu, Trạm Tấu, Yên Bái",
   "Longtitude": 21.5148504,
   "Latitude": 104.4315746
 }
];